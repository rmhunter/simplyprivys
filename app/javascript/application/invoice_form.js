global.flatpickr = require('flatpickr');

$(document).ready(function() {

    // datepickers only get initialized on $(document).ready,
    // so we need to activate datepickers on any inputs loaded via JS
    var $line_items_forms = $('#line_items_forms');
    if ($line_items_forms.length > 0) {
        $line_items_forms.on('cocoon:after-insert', function(e, insertedItem) {
            $('.date_picker', insertedItem).flatpickr({});
            $('.date_time_picker', insertedItem).flatpickr({
                enableTime: true,
            });
        });
    }

});
