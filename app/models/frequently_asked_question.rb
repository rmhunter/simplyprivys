class FrequentlyAskedQuestion < ApplicationRecord
  validates :question, presence: true
  validates :answer, presence: true
  default_scope { order :position }
end
