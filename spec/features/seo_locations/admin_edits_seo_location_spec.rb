require 'rails_helper'

RSpec.feature "Admin edits a SEO Location" do
  scenario "successfully from index" do
    seo_location = create(:seo_location)

    visit admin_seo_locations_path(as: create(:admin_user))
    edit_button(seo_location).click
    fill_in 'seo_location[name]', with: 'New SEO Location Name'
    click_on 'Save Seo Location'

    expect(page).to have_content 'SeoLocation was successfully updated'
    expect(SeoLocation.last.name).to eq 'New SEO Location Name'
  end

  scenario "successfully from show" do
    seo_location = create(:seo_location)

    visit admin_seo_locations_path(as: create(:admin_user))
    click_on seo_location.name
    edit_button(seo_location).click
    fill_in 'seo_location[name]', with: 'New SEO Location Name'
    click_on 'Save Seo Location'

    expect(page).to have_content 'SeoLocation was successfully updated'
    expect(SeoLocation.last.name).to eq 'New SEO Location Name'
  end

  def edit_button(seo_location)
    find("a[href='#{edit_admin_seo_location_path(seo_location)}']")
  end
end