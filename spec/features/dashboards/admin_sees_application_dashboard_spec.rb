require 'rails_helper'

RSpec.feature "Admin sees application dashboard page" do

  scenario 'sees a list of reservations for today' do
    reservation1 = create(:reservation, event_date: DateTime.now)
    reservation2 = create(:reservation, event_date: DateTime.now + 1.days)

    visit dashboard_path(as: create(:admin_user))

    within '#reservations_today' do
      expect(page).to have_content reservation1.name
      expect(page).not_to have_content reservation2.name
    end
  end

  scenario "sees a list of upcoming reservations" do
    reservation1 = create(:reservation, event_date: DateTime.now)
    reservation2 = create(:reservation, event_date: DateTime.now + 1.days)

    visit dashboard_path(as: create(:admin_user))

    within '#reservations_upcoming' do
      expect(page).not_to have_content reservation1.name
      expect(page).to have_content reservation2.name
    end
  end

  scenario "sees a list of reservations whose status is inquired" do
    reservation1 = create(:reservation, status: 1)
    reservation2 = create(:reservation, status: 2)

    visit dashboard_path(as: create(:admin_user))

    within '#reservations_inquiring' do
      expect(page).to have_content reservation1.name
      expect(page).not_to have_content reservation2.name
    end
  end
end