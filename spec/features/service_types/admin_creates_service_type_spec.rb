require 'rails_helper'

RSpec.feature "Admin creates a service type" do
  scenario "successfully" do
    visit admin_service_types_path(as: create(:admin_user))

    page.find('[data-role="new-service_type"]').click

    fill_in 'service_type[name]', with: "ServiceType 1"
    click_on 'Save Service Type'

    expect(page).to have_content 'ServiceType was successfully created'
    expect(page).to have_content 'ServiceType 1'
    expect(ServiceType.last.name).to eq 'ServiceType 1'
  end
end