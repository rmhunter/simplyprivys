require 'rails_helper'

RSpec.feature "Admin sees a item" do
  scenario "successfully" do
    item = create(:item)

    visit admin_items_path(as: create(:admin_user))
    click_on item.name

    expect(page).to have_content item.name
  end
end