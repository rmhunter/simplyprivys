require 'rails_helper'

<% module_namespacing do -%>
RSpec.describe <%= class_name %>, <%= type_metatag(:model) %> do
  describe 'validations' do
    pending "add any validation tests, or remove this"
    # Optionally add specific value tests to check edge-cases
    # it { should allow_values('Bob', 'Mary').for(:first_name) }
    # it { should_not allow_value(nil).for(:first_name) }
  end

  describe 'associations' do
<% attributes.each do |attr| -%>
<% if attr.reference? -%>
    it { should belong_to :<%= attr.singular_name %> }
<% end -%>
<% end -%>
    pending "add association examples, or remove this"
  end

  # test any additional methods or business logic here, for example:
  # describe '.increment!' do
  #   it "incrememts the counter" do
  #     starting_count = subject.counter
  #     subject.increment!
  #     expect(subject.count).to eq(starting_count + 1)
  #   end
  # end
end
<% end -%>
