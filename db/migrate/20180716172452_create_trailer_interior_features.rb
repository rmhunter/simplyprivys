class CreateTrailerInteriorFeatures < ActiveRecord::Migration[5.1]
  def change
    create_table :trailer_interior_features do |t|
      t.string :feature, null: false
      t.integer :position

      t.timestamps
    end
  end
end
