global.$ = require('jquery');

$(document).ready(function(){
    const NAVBAR_HEIGHT = 96;

    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            const hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top - NAVBAR_HEIGHT
            }, 800);
        }
    });

    // Collapse navbar on click
    $('.navbar-nav>li>a').on('click', function(){
        $('.navbar-collapse').collapse('hide');
    });
});
