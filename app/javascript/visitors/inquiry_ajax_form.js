global.flatpickr = require('flatpickr');
global.$ = require('jquery');
global.moment = require('moment');

$(document).ready(function() {

    var $new_inquiry_form = $('form#new_inquiry');

    if ($new_inquiry_form.length) {

        var $submit_button = $new_inquiry_form.find('[type="submit"]');

        $new_inquiry_form.on("ajax:send", function(event) {
            $submit_button.prop("disabled", true);
        });

        $new_inquiry_form.on("ajax:success", function(event) {
            var $success_modal = $('#inquiry_created_dialog');
            $success_modal.modal('show');
            $new_inquiry_form[0].reset();
            $submit_button.prop("disabled", false);
        });

        $new_inquiry_form.on("ajax:error", function(event) {
            var errors = event.detail[0];

            for(field in errors) {
                var msg = errors[field].toString();
                var $input = $new_inquiry_form.find('input[name="inquiry[' + field + ']"]');

                // activate BS4 'invalid' feedback
                $input.addClass('is-invalid');
                $input.after('<div class="invalid-feedback">' + msg + '</div>');

                // deactivate 'invalid' status on blur
                // it may still be invalid, but it will reactivate on next submit if so
                $input.blur(function(e) {
                    $(this).removeClass('is-invalid');
                    $(this).siblings('.invalid-feedback').remove();
                });
            }

            // prevent duplicate submissions caused by additional clicks / keypresses
            $submit_button.prop("disabled", false);
        });

        // initialize date picker for event_date
        flatpickr('#inquiry_event_date', {
            minDate: "today",
        });
    }
});
