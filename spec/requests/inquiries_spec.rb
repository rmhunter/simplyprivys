require 'rails_helper'

RSpec.describe "Inquiries Requests", type: :request do

  describe 'POST /inquiries.json' do
    context 'with valid parameters' do
      it 'creates the inquiry and responds success' do
        valid_attributes = attributes_for(:inquiry).compact

        post '/inquiries.json', params: {inquiry: valid_attributes}

        expect(response).to be_success
        expect(Customer.count).to eq 1
        expect(Reservation.count).to eq 1
      end
    end

    context 'with invalid parameters' do
      it 'rejects the creation and responds 422 with error messages' do
        invalid_attributes = attributes_for(:inquiry, :invalid)

        post '/inquiries.json', params: {inquiry: invalid_attributes}

        expect(response.code).to eq '422'
        expect(Customer.count).to eq 0
        expect(Reservation.count).to eq 0
      end
    end
  end

end