require 'rails_helper'

RSpec.describe 'Admin sees trailer interior features index page' do
  scenario 'successfully' do
    features = create_list(:trailer_optional_feature, 2)

    visit admin_trailer_optional_features_path(as: create(:admin_user))

    expect(page).to have_content(features.first.feature)
    expect(page).to have_content(features.second.feature)
  end
end