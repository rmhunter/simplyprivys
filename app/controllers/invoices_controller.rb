class InvoicesController < ApplicationController
  before_action :require_login
  before_action :require_admin

  def index
    @invoices = Invoice.page(params[:page])
  end

  def show
    @invoice = Invoice.find(params[:id])
  end

  def new
    @invoice = Invoice.new(new_invoice_params)
    @invoice.line_items.build unless @invoice.line_items.count > 0
  end

  def create
    @invoice = Invoice.new(invoice_params)
    if @invoice.save
      redirect_to @invoice, notice: 'Invoice created successfully'
    else
      render :new
    end
  end

  def edit
    @invoice = Invoice.find(params[:id])
  end

  def update
    @invoice = Invoice.find(params[:id])
    if @invoice.update(invoice_params)
      redirect_to @invoice, notice: 'Invoice updated successfully'
    else
      render :edit
    end
  end

  def destroy
    @invoice = Invoice.find(params[:id])
    @invoice.destroy
    flash[:notice] = 'Invoice deleted successfully' if @invoice.destroyed?
    redirect_to invoices_path
  end

  private

  def invoice_params
    params.require(:invoice).permit(
      :reservation_id, :status, :due_on, :sent_at, :description,
      line_items_attributes: [:id, :_destroy, # these are required by Cocoon
                              :completed_on, :description, :quantity,
                              :price, :discount_percent, :tax_percent]
    )
  end

  def new_invoice_params
    params.permit(:reservation_id)
  end
end
