require 'rails_helper'

RSpec.feature 'Admin adds a rental to a reservation' do
  scenario 'sees form for new rental' do
    reservation = create(:reservation)

    visit reservation_path(reservation, as: create(:admin_user))
    click_on 'Add Rental'

    expect(current_path).to eq new_reservation_rental_path(reservation)
    expect(page).to have_content('Item')
    expect(page).to have_content('Duration type')
    expect(page).to have_content('Start time')
    expect(page).to have_content('End time')
  end

  scenario 'sees pre-populated line item form after saving rental', js: true do
    reservation = create(:reservation)
    item = create(:item)
    start_time = 7.days.from_now.change(hour: 9, min: 9)
    end_time = 9.days.from_now.change(hour: 17, min: 9)

    visit reservation_path(reservation, as: create(:admin_user))
    click_on 'Add Rental'
    select item.name, from: 'rental_item_id'
    fill_in_date_picker 'rental_start_time', with: start_time
    fill_in_date_picker 'rental_end_time', with: end_time
    click_on 'Continue'

    rental = Rental.last
    expect(current_path).to eq new_rental_line_item_path(rental)
    expect(find_field('line_item_description').value).to eq rental.billing_description
    expect(find_field('line_item_quantity').value).to eq rental.billing_quantity.to_s
    expect(find_field('line_item_price').value).to eq rental.billing_price.to_s
    expect(find_field('line_item_discount_percent').value).to eq '0.0'
    expect(find_field('line_item_tax_percent').value).to eq '8.45'
  end

  scenario 'successfully creates both rental and line item', js: true do
    reservation = create(:reservation)
    item = create(:item)
    start_time = 7.days.from_now.change(hour: 9, min: 9)
    end_time = 9.days.from_now.change(hour: 17, min: 9)

    visit reservation_path(reservation, as: create(:admin_user))
    click_on 'Add Rental'
    select item.name, from: 'rental_item_id'
    fill_in_date_picker 'rental_start_time', with: start_time
    fill_in_date_picker 'rental_end_time', with: end_time
    click_on 'Continue'
    # now we're on the line_items#new form
    fill_in 'line_item_discount_percent', with: '25'
    click_on 'Save'

    rental = Rental.last
    expect(current_path).to eq reservation_path(rental.reservation)
    expect(page).to have_content rental.line_item.description
    expect(rental.line_item.discount_percent).to eq 25
    expect(rental.line_item.description).to eq rental.billing_description
  end
end
