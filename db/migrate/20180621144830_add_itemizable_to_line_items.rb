class AddItemizableToLineItems < ActiveRecord::Migration[5.1]
  def change
    add_reference :line_items, :itemizable, polymorphic: true, index: true
  end
end
