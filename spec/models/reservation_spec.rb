require 'rails_helper'

RSpec.describe Reservation, type: :model do
  describe "validations" do
    it { should define_enum_for(:status).with([:created, :inquired, :quoted,
                                               :confirmed, :active, :completed, :cancelled]) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:customer) }
  end

  describe "associations" do
    it { should belong_to(:customer) }
    it { should have_many(:services) }
    it { should have_many(:rentals) }
    it { should have_one(:invoice) }
  end

  describe 'after_commit' do
    it 'ensures each reservation has an invoice' do
      reservation = create(:reservation)
      expect(reservation.invoice).not_to eq nil
    end
  end

  describe '#first_delivery_time' do
    it 'returns the first delivery time of all rentals in the reservation' do
      reservation = create(:reservation)
      latest_rental = create(:rental, reservation: reservation, start_time: 4.days.from_now.change(usec: 0))
      early_rental = create(:rental, reservation: reservation, start_time: 1.day.from_now.change(usec: 0))
      later_rental = create(:rental, reservation: reservation, start_time: 2.days.from_now.change(usec: 0))
      expect(reservation.first_delivery_time).to eq early_rental.start_time
    end
  end

  describe '#last_pickup_time' do
    it 'returns the last pickup time of all rentals in the reservation' do
      reservation = create(:reservation)
      latest_rental = create(:rental, reservation: reservation, start_time: 4.days.from_now.change(usec: 0))
      early_rental = create(:rental, reservation: reservation, start_time: 1.day.from_now.change(usec: 0))
      later_rental = create(:rental, reservation: reservation, start_time: 2.days.from_now.change(usec: 0))
      expect(reservation.last_pickup_time).to eq latest_rental.end_time
    end
  end
end
