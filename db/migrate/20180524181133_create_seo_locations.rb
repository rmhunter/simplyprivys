class CreateSeoLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :seo_locations do |t|
      t.string :name, null: false
      t.string :slug, null: false

      t.timestamps

      t.index :slug, unique: true
    end
  end
end
