module Admin
  class ContractTermsController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = ContractTerm.
    #     page(params[:page]).
    #     per(10)
    # end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   ContractTerm.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information
    def index
      super
      @resources = ContractTerm.all
    end

    def sort
      params[:contract_term].each_with_index do |id, index|
        ContractTerm.where(id: id).update_all(position: index + 1)
      end
      head :ok
    end
  end
end
