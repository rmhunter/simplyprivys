class ApplicationController < ActionController::Base
  include Clearance::Controller
  protect_from_forgery with: :exception

  private

  def require_admin
    redirect_to root_path unless current_user.admin?
  end
end
