class InquiryMailer < ApplicationMailer
  def customer_receipt(inquiry)
    @inquiry = inquiry
    mail to: inquiry.customer.email, subject: 'Your Simply Privys Inquiry'
  end

  def admin_notification(inquiry)
    @inquiry = inquiry
    mail to: ENV['admin_email'], subject: 'New Inquiry'
  end
end
