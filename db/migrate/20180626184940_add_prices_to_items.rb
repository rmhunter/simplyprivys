class AddPricesToItems < ActiveRecord::Migration[5.1]
  def change
    add_column :items, :price_per_week, :decimal, precision: 16, scale: 2, default: "0.0", null: false
    add_column :items, :price_per_month, :decimal, precision: 16, scale: 2, default: "0.0", null: false
  end
end
