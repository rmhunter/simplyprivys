require "rails_helper"

RSpec.feature "Visitor submits an inquiry", js: true do
  context "with only required fields" do
    scenario "and sees success message" do
      first_name = 'Bob'
      last_name = 'Smith'
      email = 'bob@example.com'
      phone = '555-555-5555'

      visit root_path
      fill_in 'inquiry_first_name', with: first_name
      fill_in 'inquiry_last_name', with: last_name
      fill_in 'inquiry_email', with: email
      fill_in 'inquiry_phone', with: phone
      submit_form

      expect(page).to have_content "Thank you"
      customer = Customer.last
      expect(customer.first_name).to eq first_name
      expect(customer.last_name).to eq last_name
      expect(customer.email).to eq email
      expect(customer.phone).to eq phone
      reservation = Reservation.last
      expect(reservation.customer).to eq customer
      expect(reservation.status).to eq 'inquired'
    end

    scenario "and the server sends email notifications to customer and admin" do
      deliveries = ActionMailer::Base.deliveries.count
      first_name = 'Bob'
      last_name = 'Smith'
      email = 'bob@example.com'
      phone = '555-555-5555'

      visit root_path
      fill_in 'inquiry_first_name', with: first_name
      fill_in 'inquiry_last_name', with: last_name
      fill_in 'inquiry_email', with: email
      fill_in 'inquiry_phone', with: phone
      submit_form

      sleep 1
      expect(ActionMailer::Base.deliveries.count).to eq (deliveries + 2)
    end
  end

  context "with all fields" do
    scenario 'handled correctly' do
      first_name = 'Bob'
      last_name = 'Smith'
      email = 'bob@example.com'
      phone = '555-555-5555'
      company_name = 'Awesome Company'
      event_date = 2.weeks.from_now.to_date
      event_type = 'Retreat'
      event_location = 'Awesometown, USA'
      number_of_guests = '200'
      additional_notes = 'Need your most awesome trailer'

      visit root_path
      fill_in 'inquiry_first_name', with: first_name
      fill_in 'inquiry_last_name', with: last_name
      fill_in 'inquiry_email', with: email
      fill_in 'inquiry_phone', with: phone
      fill_in 'inquiry_company_name', with: company_name
      fill_in_date_picker 'inquiry_event_date', with: event_date
      fill_in 'inquiry_event_type', with: event_type
      fill_in 'inquiry_event_location', with: event_location
      fill_in 'inquiry_number_of_guests', with: number_of_guests
      fill_in 'inquiry_additional_notes', with: additional_notes
      submit_form

      expect(page).to have_content "Thank you"
      customer = Customer.last
      expect(customer.first_name).to eq first_name
      expect(customer.last_name).to eq last_name
      expect(customer.email).to eq email
      expect(customer.phone).to eq phone
      expect(customer.company_name).to eq company_name
      reservation = Reservation.last
      expect(reservation.name).to eq "#{company_name} #{event_type}"
      expect(reservation.customer).to eq customer
      expect(reservation.status).to eq 'inquired'
      expect(reservation.event_date).to eq event_date
      expect(reservation.event_type).to eq event_type
      expect(reservation.event_location).to eq event_location
      expect(reservation.number_of_guests).to eq number_of_guests
      expect(reservation.additional_notes).to eq additional_notes
    end
  end

  context "without required fields" do
    scenario "and sees validation message(s)" do
      visit root_path

      submit_form

      expect(page).to have_content "can't be blank"
      expect(Customer.count).to eq 0
      expect(Reservation.count).to eq 0
    end
  end

  def submit_form
    click_on 'Get in Touch'
  end
end
