class AddEventDateToReservations < ActiveRecord::Migration[5.1]
  def change
    add_column :reservations, :event_date, :date

    remove_column :reservations, :requested_delivery_time, :datetime
    remove_column :reservations, :requested_pickup_time, :datetime
  end
end
