require 'rails_helper'

RSpec.feature "Admin edits a customer" do
  scenario "successfully from index" do
    customer = create(:customer, :with_all_fields)

    visit customers_path(as: create(:admin_user))

    find("#customer_#{customer.id}").click_on 'Edit'
    fill_in 'customer[first_name]', with: 'NewBob'
    fill_in 'customer[last_name]', with: 'NewDob'
    fill_in 'customer[email]', with: 'newbob@example.com'
    click_on 'Save Customer'

    expect(page).to have_content 'Customer has been updated'
    expect(customer.reload.first_name).to eq 'NewBob'
    expect(customer.reload.last_name).to eq 'NewDob'
    expect(customer.reload.email).to eq 'newbob@example.com'
  end

  scenario "successfully from show" do
    customer = create(:customer, :with_all_fields)

    visit customers_path(as: create(:admin_user))

    click_on customer.email
    page.find('.edit-customer-button').click
    fill_in 'customer[first_name]', with: 'NewBob'
    fill_in 'customer[last_name]', with: 'NewDob'
    fill_in 'customer[email]', with: 'newbob@example.com'
    click_on 'Save Customer'

    expect(page).to have_content 'Customer has been updated'
    expect(customer.reload.first_name).to eq 'NewBob'
    expect(customer.reload.last_name).to eq 'NewDob'
    expect(customer.reload.email).to eq 'newbob@example.com'
  end
end
