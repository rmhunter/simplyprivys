/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

// Stylesheet Dependencies
import "bootstrap/dist/css/bootstrap.min.css";
import "mdbootstrap/css/mdb.min.css";
import "@fortawesome/fontawesome/styles.css";
import "bootstrap-daterangepicker-master/daterangepicker.css";
import "flatpickr/dist/flatpickr.min.css"

// Custom Styles
import "../shared/global.css";
import "../application/application.css";
import "../application/sidebar.css";
import "../application/flatpickr.css";

// JavaScript Dependencies
import "jquery";
import "jquery-ui/ui/widget"
import "jquery-ui/ui/widgets/sortable"
import "popper.js/dist/popper.min";
import "node-waves";
import "bootstrap/dist/js/bootstrap.min";
import "mdbootstrap/js/mdb.min.js"
import "@fortawesome/fontawesome/index";
import "@fortawesome/fontawesome-free-regular/index";
import "@fortawesome/fontawesome-free-solid/index";
import "@fortawesome/fontawesome-free-brands/index";
import "moment";
import "bootstrap-daterangepicker-master/daterangepicker";
import "flatpickr";
import "cocoon-js";

// Custom Javascript
import "../application/reservations_daterangepicker";
import "../application/sidebar.js";
import "../application/date_picker";
import "../application/invoice_form";
import "../application/drag-drop-sortable"

console.log('Hello World from `application` pack via Webpacker');
