class CreateReservationServices < ActiveRecord::Migration[5.1]
  def change
    create_table :reservation_services do |t|
      t.references :reservation, foreign_key: true
      t.references :service, foreign_key: true
      t.string :name, null: false
      t.string :description
      t.decimal :price, precision: 16, scale: 2, null: false, default: 0

      t.timestamps
    end
  end
end
