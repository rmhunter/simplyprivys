class Inquiry
  include ActiveModel::Model

  attr_accessor :first_name, :last_name, :email, :phone,
                :company_name, :event_date, :event_type, :event_location,
                :number_of_guests, :additional_notes
  attr_reader :customer, :reservation

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true
  validates :phone, presence: true

  def initialize(attributes={})
    super
    convert_dates_if_necessary
  end

  def save
    saved = false
    if valid?
      ActiveRecord::Base.transaction do
        @customer = Customer.find_or_initialize_by(email: self.email)
        @customer.first_name = @customer.first_name || self.first_name
        @customer.last_name = @customer.last_name || self.last_name
        @customer.phone = @customer.phone || self.phone
        @customer.company_name = @customer.company_name || self.company_name
        @customer.save!

        @reservation = @customer.reservations.create!(
            name: default_reservation_name,
            status: 'inquired',
            event_date: self.event_date,
            event_type: self.event_type,
            event_location: self.event_location,
            number_of_guests: self.number_of_guests,
            additional_notes: self.additional_notes,
        )
      end

      saved = (@customer.persisted? && @reservation.persisted?)

      if saved
        send_customer_receipt
        send_admin_notification
      end
    end
    # return boolean whether it was persisted successfully, just like if it were an AR object
    saved
  end
  alias_method :save!, :save

  def send_customer_receipt
    InquiryMailer.customer_receipt(self).deliver_now
  end

  def send_admin_notification
    InquiryMailer.admin_notification(self).deliver_now
  end

  def event_date=(new_event_date)
    @event_date = new_event_date.to_date
  end

  private

  def default_reservation_name
    if self.company_name.present?
      customer_name_for_reservation = self.company_name
    else
      customer_name_for_reservation = "#{self.first_name} #{self.last_name}"
    end
    [customer_name_for_reservation, self.event_type].compact.join(' ')
  end

  def convert_dates_if_necessary
    @event_date = @event_date.to_date if @event_date.present? && @event_date.is_a?(String)
  end
end
