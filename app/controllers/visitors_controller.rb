class VisitorsController < ApplicationController
  layout 'visitor/application'
  def index
    @inquiry = Inquiry.new
    @frequently_asked_questions = FrequentlyAskedQuestion.all
    @trailer_interior_features = TrailerInteriorFeature.all
    @trailer_optional_features = TrailerOptionalFeature.all

    if params[:slug].present?
      begin
        @seo_location = SeoLocation.friendly.find(params[:slug])
      rescue ActiveRecord::RecordNotFound => e
        if signed_in?
          redirect_to admin_seo_locations_path, alert: 'Location not found, create one here or try again.'
        else
          redirect_to landing_page_path
        end
      end
    end
  end
end
