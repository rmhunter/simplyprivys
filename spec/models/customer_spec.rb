require 'rails_helper'

RSpec.describe Customer, type: :model do
  describe "validations" do
  end

  describe "associations" do
    it { should have_many :reservations }
  end

  describe 'destroy' do
    it 'should soft-delete the record' do
      customer = create(:customer)
      customer.destroy
      expect(customer.deleted_at).not_to be_nil
    end
  end

  describe 'delete' do
    it 'should soft-delete the record' do
      customer = create(:customer)
      customer.delete
      expect(customer.deleted_at).not_to be_nil
    end
  end


  describe '#full_name' do
    it 'returns the first and last name of the Customer' do
      customer = build(:customer, first_name: 'Bob', last_name: 'Smith')

      expect(customer.full_name).to eq 'Bob Smith'
    end

    it 'gracefully handles null values' do
      first_name_only = build(:customer, first_name: 'Bob', last_name: nil)
      last_name_only = build(:customer, first_name: nil, last_name: 'Smith')

      expect(first_name_only.full_name).to eq 'Bob'
      expect(last_name_only.full_name).to eq 'Smith'
    end
  end

  describe '#name_for_reservation' do
    it 'returns the company name if present' do
      customer = build(:customer, company_name: 'Awesome Company')
      expect(customer.name_for_reservation).to eq customer.company_name
    end

    it 'falls back to full_name if no company name' do
      customer = build(:customer)
      expect(customer.name_for_reservation).to eq customer.full_name
    end
  end
end
