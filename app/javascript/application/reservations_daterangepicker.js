/**
 * This file is where we put the configuration for any daterangepicker inputs.
 * see http://www.daterangepicker.com/#usage
 */
global.$ = require('jquery')
global.moment = require('moment')

$(document).ready(function() {

    /*======================================================================================================================
        Date range form on reservations page
    ======================================================================================================================*/
    var reservationsPageDateRangePicker = $('input#reservation_daterangepicker');
    var inputDisplayFormat = 'MMM D h:mm A';
    reservationsPageDateRangePicker.daterangepicker({
        opens: 'left',
        minDate: moment(),
        showDropdowns: true,
        autoUpdateInput: false, // hack to allow empty input initially (requires the apply event handler below)
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'MMM D h:mm A'
        }
    }, function(start, end, label) {
        console.log("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    });

    // upon 'applying' daterangepicker input, copy the values into hidden fields for the Rails model
    reservationsPageDateRangePicker.on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format(inputDisplayFormat) + ' - ' + picker.endDate.format(inputDisplayFormat));
        $('#reservation_requested_delivery_time').val(picker.startDate.toISOString());
        $('#reservation_requested_pickup_time').val(picker.endDate.toISOString());
    });

});
