class AddPositionToContractTerms < ActiveRecord::Migration[5.1]
  def change
    add_column :contract_terms, :position, :integer
  end
end
