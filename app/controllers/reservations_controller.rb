class ReservationsController < ApplicationController
  before_action :require_login
  before_action :require_admin

  def index
    @reservations = Reservation.all
  end

  def show
    @reservation = Reservation.find(params[:id])
    @rentals = Rental.where('reservation_id = ?', params[:id])
    @rental = Rental.new
  end

  def new
    @reservation_form = ReservationForm.new(new_reservation_params)
  end

  def edit
    @reservation = Reservation.find(params[:id])
  end

  def create
    @reservation_form = ReservationForm.new(reservation_form_params)
    if @reservation_form.save
      flash[:notice] = 'Reservation has been created'
      redirect_to @reservation_form.reservation
    else
      flash[:danger] = 'Reservation has not been created'
      render 'new'
    end
  end

  def update
    @reservation = Reservation.find(params[:id])
    if @reservation.update(reservation_params)
      flash[:notice] = 'Reservation has been updated'
      redirect_to @reservation
    else
      flash[:danger] = 'Reservation has not been updated'
      render 'edit'
    end
  end

  def destroy
    @reservation = Reservation.find(params[:id])
    if @reservation.destroy
      flash[:notice] = 'Reservation has been deleted'
      redirect_to reservations_path
    end
  end

  private

  def reservation_params
    params.require(:reservation).permit(:customer_id, :name, :description, :status,
                                        :event_date, :event_location, :number_of_guests,
                                        :additional_notes, :water_on_site, :power_on_site)
  end

  def new_reservation_params
    params.permit(:customer_id)
  end

  def reservation_form_params
    params.require(:reservation_form).permit(:customer_id, :customer_first_name, :customer_last_name, :customer_email, :customer_phone,
                                             :event_date, :event_type, :event_location, :number_of_guests,
                                             :power_on_site, :water_on_site, :additional_notes)
  end
end
