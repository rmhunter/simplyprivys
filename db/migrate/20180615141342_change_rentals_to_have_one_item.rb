class ChangeRentalsToHaveOneItem < ActiveRecord::Migration[5.1]
  def change
    drop_join_table :items, :rentals do |t|
      t.index [:item_id, :rental_id]
    end

    add_reference :rentals, :item, index: true
  end
end
