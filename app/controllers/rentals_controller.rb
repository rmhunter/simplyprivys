class RentalsController < ApplicationController
  before_action :require_login
  before_action :require_admin

  def new
    @rental = Rental.new(new_rental_params)
  end

  def create
    @rental = Rental.new(rental_params)

    if @rental.save
      redirect_to new_rental_line_item_path(@rental)
    else
      render :new
    end
  end

  def edit
    @rental = Rental.find(params[:id])
  end

  def update
    @rental = Rental.find(params[:id])
    if @rental.update(rental_params)
      if @rental.line_item.present?
        redirect_to edit_line_item_path(@rental.line_item)
      else
        redirect_to new_rental_line_item_path(@rental)
      end
    else
      render :edit
    end
  end

  def destroy
    @rental = Rental.find(params[:id])
    if @rental.destroy
      flash[:notice] = 'Rental has been deleted'
      redirect_to @rental.reservation
    end
  end

  def rental_params
    params.require(:rental).permit(:start_time, :end_time, :item_id, :reservation_id)
  end

  def new_rental_params
    params.permit(:reservation_id)
  end
end
