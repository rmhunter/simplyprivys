class Rental < ApplicationRecord
  enum duration_type: {daily: 0, weekly: 1, monthly: 2}
  belongs_to :reservation
  belongs_to :item
  has_one :line_item, as: :itemizable, dependent: :destroy
  validates :item, presence: true

  def billing_description
    "Rental: #{item.name} (#{duration_type})"
  end

  def billing_quantity
    return 1 unless start_time.present? && end_time.present?
    [duration_in_days.round, 1].max
  end

  def billing_price
    item.price_per_day
  end

  def billing_tax_rate
    8.45
  end

  def to_s
    billing_description
  end

  private

  def duration_in_days
    (end_time - start_time) / 1.day
  end
end
