# Load default configuration data
Rake::Task['sp:db:seed'].invoke

# Load sample data by default on development environment
Rake::Task['sp:db:sample'].invoke if Rails.env.development?
