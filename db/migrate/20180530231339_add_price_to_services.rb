class AddPriceToServices < ActiveRecord::Migration[5.1]
  def change
    add_column :services, :price, :decimal, precision: 16, scale: 2, default: 0, null: false
  end
end
