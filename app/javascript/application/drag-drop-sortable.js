import * as Rails from "rails-ujs";

$(document).ready(function() {

    $('.drag-drop-sortable').sortable({

        helper: function(e, tr)
        {
            let $originals = tr.children();
            let $helper = tr.clone();
            $helper.children().each(function(index)
            {
                // Set helper cell sizes to match the original sizes
                $(this).width($originals.eq(index).width());
            });
            return $helper;
        },
        handle: '.sort-handle',
        axis: "y",
        containment: "parent",
        cursor: "move",
        tolerance: 'pointer',

        update: function() {
            Rails.ajax({
                url: $(this).data("url"),
                type: "PATCH",
                data: $(this).sortable('serialize'),

        });
        }
    });

});
