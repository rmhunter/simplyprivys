require 'rails_helper'

RSpec.feature "Admin edits a reservation" do
  scenario "via reservation index" do
    reservation = create(:reservation)

    visit reservations_path(as: create(:admin_user))
    find("#reservation_#{reservation.id}").click_on 'Edit'
    fill_in 'reservation[name]', with: 'Changed Name'
    click_on 'Save Reservation'

    expect(page).to have_content 'Reservation has been updated'
    expect(page).to have_content 'Changed Name'
  end

  scenario "via reservation view" do
    reservation = create(:reservation)

    visit reservation_path(reservation, as: create(:admin_user))
    click_on 'Edit Reservation'
    fill_in 'reservation[name]', with: 'Changed Name'
    click_on 'Save Reservation'

    expect(page).to have_content 'Reservation has been updated'
    expect(page).to have_content 'Changed Name'
  end
end
