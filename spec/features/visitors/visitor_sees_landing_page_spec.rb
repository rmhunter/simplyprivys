require "rails_helper"

RSpec.feature "Visitor sees landing page" do
  scenario "successfully" do
    visit root_path

    expect(page).to have_css '#hero'
    expect(page).to have_css '#how_it_works'
    expect(page).to have_css '#trailers'
    expect(page).to have_css '#faq'
    expect(page).to have_css '#contact_us'
  end
end
