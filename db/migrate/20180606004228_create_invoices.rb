class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.references :reservation, foreign_key: true
      t.date :due_on
      t.datetime :sent_at
      t.integer :status, null: false, default: 0 # enum
      t.string :description

      t.timestamps
    end
    add_index :invoices, :due_on
    add_index :invoices, :status
  end
end
