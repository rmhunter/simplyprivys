module ButtonHelper
  def icon_button(path, options = {})
    icon = options.delete(:icon) || 'info'
    text = options.delete(:text)
    color = options.delete(:color) || 'default'
    classes =  ['btn', "btn-#{color}", options.delete(:class)]
    class_string = classes.compact.join(' ')
    options[:class] = class_string
    link_to fa_icon(icon, text: text), path, options
  end

  def back_button(path = :back, options={})
    options[:icon] ||= 'chevron-left'
    options[:text] ||= 'Back'
    options[:color] ||= 'light'
    icon_button path, options
  end

  def cancel_button(path = :back, options = {})
    options[:icon] ||= 'times'
    options[:text] ||= 'Cancel'
    options[:color] ||= 'light'
    icon_button path, options
  end

  def new_button(path, options={})
    options[:icon] ||= 'plus'
    options[:color] ||= 'primary'
    icon_button path, options
  end

  def edit_button(path, options={})
    options[:icon] ||= 'pencil-alt'
    options[:text] ||= 'Edit'
    options[:color] ||= 'warning'
    icon_button path, options
  end

  def icon_link(path, options = {})
    icon = options.delete(:icon) || 'info'
    text = options.delete(:text)
    color = options.delete(:color) || 'info'
    classes =  ["text-#{color}", options.delete(:class)]
    class_string = classes.compact.join(' ')
    options[:class] = class_string
    link_to fa_icon(icon, text: text), path, options
  end

  def edit_link_in_table(path, options={})
    options[:icon] ||= 'pencil-alt'
    options[:text] ||= 'Edit'
    options[:color] ||= 'info'
    icon_link path, options
  end

  def delete_link_in_table(path, options={})
    options[:icon] ||= 'trash'
    options[:text] ||= 'Delete'
    options[:color] ||= 'danger'
    options[:method] ||= :delete
    options[:data] = (options[:data] || {}).merge!({confirm: "Are you sure you want to delete this record?"})
    icon_link path, options
  end

  def submit_button(options={})
    f = options.delete(:f)
    classes =  ['btn', 'btn-default', options.delete(:class)]
    text = options.delete(:text) || 'Save'
    icon = options.delete(:icon) || 'check'
    class_string = classes.compact.join(' ')
    data_hash = options.delete(:data) || {}
    f.button :button, fa_icon(icon, text: text), type: :submit, class: class_string, data: data_hash
  end
end
