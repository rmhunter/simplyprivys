source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


gem 'rails', '~> 5.1.6'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.7'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'

gem 'webpacker'
gem 'foreman'
gem 'figaro'
gem 'simple_form', '~> 4.0'
gem 'clearance'
gem 'administrate'
gem 'administrate-field-password'
gem 'kaminari'
gem 'bootstrap4-kaminari-views'
gem 'friendly_id', '~> 5.1'
gem 'sitemap_generator', '~> 6.0'
gem 'aws-sdk'
gem 'cocoon', '~> 1.2'
gem 'colorize'
gem 'acts_as_list'
gem 'paranoia', '~> 2.2'
gem 'virtus'

gem 'sendgrid-ruby', group: :production

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  gem 'rspec-rails'
  gem 'rails-controller-testing'
  gem 'poltergeist'
  gem 'factory_bot_rails'
  gem 'pry-rails'
  gem 'pry-byebug'
  gem 'pry-stack_explorer'
end

group :test do
  gem 'capybara'
  gem 'shoulda-matchers'
  gem 'database_cleaner'
  gem 'selenium-webdriver'
  gem "chromedriver-helper"
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'spring-commands-rspec'
  gem 'guard-rails', require: false
  gem 'guard-rspec', require: false
  gem 'terminal-notifier-guard', require: false
  gem 'rails_real_favicon'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
