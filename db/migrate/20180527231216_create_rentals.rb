class CreateRentals < ActiveRecord::Migration[5.1]
  def change
    create_table :rentals do |t|
      t.datetime :delivery_time
      t.datetime :pickup_time
      t.references :reservation, index: true

      t.timestamps
    end
  end
end
