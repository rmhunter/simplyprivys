namespace :sp do
  namespace :db do
    desc "Generate small amount of sample data for use in development or staging"
    task sample: :environment do

    #=====================================================================
    # User(s)
    #=====================================================================
      print "Generating sample admin user..."
      admin_user = User.find_or_initialize_by(email: 'admin@example.com') do |user|
        user.password = 'password'
        user.admin = true
        user.save!
      end
      puts "success".green

      print "Generating sample normal user..."
      normal_user = User.find_or_initialize_by(email: 'user@example.com') do |user|
        user.password = 'password'
        user.save!
      end
      puts "success".green

    #=====================================================================
    # SEO Location(s)
    #=====================================================================
      print "Generating sample SEO locations..."
      seo_location_names = ['Park City, UT', 'Salt Lake City, UT', 'Moab, UT', 'Utah']
      seo_location_names.each do |seo_location_name|
        SeoLocation.find_or_create_by!(name: seo_location_name)
      end
      puts "success".green

    #=====================================================================
    # Item(s)
    #=====================================================================
      # these are handled in seed

    #=====================================================================
    # Service Type(s)
    #=====================================================================
      # these are handled in seed

    #=====================================================================
    # Contract Term(s)
    #=====================================================================
    # these are handled in seed

    #=====================================================================
    # Inquiries (Customers and Reservations)
    #=====================================================================
      print "Generating sample inquiries..."
      past_inquiry = Inquiry.new(
          first_name: 'Suzy',
          last_name: 'Testerton',
          email: 'suzy@example.com',
          phone: '123-123-1234',
          event_date: 14.days.ago,
          event_type: 'Wedding',
          event_location: 'Random Field, Middle of Nowhere',
          number_of_guests: '150',
          additional_notes: 'Call for directions',
      )
      past_inquiry.save!

      future_inquiry = Inquiry.new(
          first_name: 'Bob',
          last_name: 'McTest',
          email: 'bob@example.com',
          phone: '555-555-5555',
          company_name: 'Awesome Festivals, Inc.',
          event_date: 1.week.from_now,
          event_type: 'Festival',
          event_location: '123 Awesome St., Awesomeville, US',
          number_of_guests: '500-1000',
          additional_notes: 'We probably need multiple of your biggest trailer.',
      )
      future_inquiry.save!
      puts "success".green

    #=====================================================================
    # Rentals within Reservation
    #=====================================================================\
      print "Generating sample rentals..."
      past_inquiry.reservation.rentals.create!(
        start_time: past_inquiry.reservation.event_date.change(hour: 9),
        end_time: past_inquiry.reservation.event_date.change(hour: 17),
        item: Item.first,
      )
      future_inquiry.reservation.rentals.create!(
        start_time: future_inquiry.reservation.event_date.change(hour: 9),
        end_time: future_inquiry.reservation.event_date.change(hour: 17),
        item: Item.second,
      )
      puts "success".green

    #=====================================================================
    # Services within Reservation
    #=====================================================================
      print "Generating sample services..."
      past_reservation = past_inquiry.reservation
      past_reservation.services.create!(
        service_type: ServiceType.first,
      )
      future_reservation = future_inquiry.reservation
      future_reservation.services.create!(
        service_type: ServiceType.second,
      )
      puts "success".green

    #=====================================================================
    # Invoices
    #=====================================================================
      # past_invoice = past_reservation.invoices.create!
      # past_invoice.line_items.create!(completed_on: past_invoice.created_at, description: 'Misc Fee',
      #                                 quantity: 1, price: 50.00)
      # future_invoice = future_reservation.invoices.create!
      # future_invoice.line_items.create!(completed_on: past_invoice.created_at, description: 'Waived Fee',
      #                                 quantity: 1, price: 50.00, discount_percent: 100.00)

    end
  end
end
