class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.string :name, null: false
      t.string :description
      t.datetime :requested_delivery_time
      t.datetime :requested_pickup_time
      t.references :customer, index: true

      t.timestamps
    end
  end
end
