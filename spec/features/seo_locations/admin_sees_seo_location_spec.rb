require 'rails_helper'

RSpec.feature "Admin sees an SEO Location" do
  scenario "successfully" do
    seo_location = create(:seo_location)

    visit admin_seo_locations_path(as: create(:admin_user))
    click_on seo_location.name

    expect(page).to have_content seo_location.name
  end
end