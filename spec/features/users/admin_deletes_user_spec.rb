require 'rails_helper'

RSpec.feature "Admin deletes an user", js: true do
  scenario "successfully" do
    user_to_delete = create(:user)

    visit admin_users_path(as: create(:admin_user))
    accept_confirm do
      delete_button(user_to_delete).click
    end

    expect(page).to have_content 'User was successfully destroyed'
  end

  def delete_button(user_to_delete)
    within "tr[data-url='#{admin_user_path(user_to_delete)}']" do
      find('[data-method=delete]')
    end
  end
end