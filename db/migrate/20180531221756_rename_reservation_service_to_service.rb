class RenameReservationServiceToService < ActiveRecord::Migration[5.1]
  def change
    rename_table :reservation_services, :services
  end
end
