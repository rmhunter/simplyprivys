require 'rails_helper'

RSpec.describe LineItem, type: :model do
  describe 'validations' do
    it { should validate_presence_of :invoice }

    it { should validate_presence_of :quantity }
    it { should validate_numericality_of(:quantity) }

    it { should validate_presence_of :price }
    it { should validate_numericality_of(:price) }

    it { should validate_presence_of :discount_percent }
    it do
      should validate_numericality_of(:discount_percent)
                 .is_greater_than_or_equal_to(0)
                 .is_less_than_or_equal_to(100)
    end

    it { should validate_presence_of :tax_percent }
    it do
      should validate_numericality_of(:tax_percent)
                 .is_greater_than_or_equal_to(0)
                 .is_less_than_or_equal_to(100)
    end
  end

  describe 'associations' do
    it { should belong_to(:invoice).touch(true) }
  end

  describe 'pre_discount_subtotal' do
    it 'returns the product of a line item\'s quantity and price' do
      line_item = create(:line_item, :with_discount, :with_sales_tax)
      expect(line_item.pre_discount_subtotal).to eq (line_item.quantity * line_item.price).round(2)
    end
  end

  describe 'discount_amount' do
    it 'returns the calculated discount amount for the line item' do
      line_item = create(:line_item, :with_discount)
      expect(line_item.discount_amount).to eq((line_item.pre_discount_subtotal * (line_item.discount_percent / 100)).round(2))
    end
  end

  describe 'post_discount_subtotal' do
    it 'returns the subtotal after applying the discount' do
      line_item = create(:line_item, :with_discount)
      expect(line_item.post_discount_subtotal).to eq(line_item.pre_discount_subtotal - line_item.discount_amount)
    end

    it 'does not include tax' do
      line_item = create(:line_item, :with_discount, :with_sales_tax)
      expect(line_item.post_discount_subtotal).to eq(line_item.pre_discount_subtotal - line_item.discount_amount)
    end
  end

  describe 'pre_tax_subtotal' do
    it 'returns the post-discount subtotal to be used to calculate tax' do
      line_item = create(:line_item)
      expect(line_item.pre_tax_subtotal).to eq(line_item.post_discount_subtotal)
    end
  end

  describe 'tax_amount' do
    it 'returns the tax amount for the line item' do
      line_item = create(:line_item, :with_sales_tax)
      expect(line_item.tax_amount).to eq((line_item.pre_tax_subtotal * (line_item.tax_percent / 100)).round(2))
    end
  end

  describe 'post_tax_subtotal' do
    it 'returns the subtotal of the line item including tax' do
      line_item = create(:line_item, :with_sales_tax)
      expect(line_item.post_tax_subtotal).to eq(line_item.pre_tax_subtotal + line_item.tax_amount)
    end
  end

  describe 'total' do
    it 'returns the net total of the line item' do
      line_item = create(:line_item)
      expect(line_item.total).to eq (line_item.quantity * line_item.price).round(2)
    end

    it 'handles discounts' do
      line_item = create(:line_item, :with_discount)
      expect(line_item.total).to eq line_item.post_discount_subtotal
    end

    it 'handles tax' do
      line_item = create(:line_item, :with_sales_tax)
      expect(line_item.total).to eq line_item.post_tax_subtotal
    end

    it 'handles discounts and taxes together, in that order' do
      line_item = create(:line_item, :with_discount, :with_sales_tax)
      expect(line_item.total).to eq line_item.post_tax_subtotal
    end
  end

  describe 'calculations' do
    it 'handles basic line item' do
      line_item = create(:line_item, quantity: 2, price: 10.00)
      expect(line_item.pre_discount_subtotal).to eq 20.00
      expect(line_item.discount_amount).to eq 0.00
      expect(line_item.post_discount_subtotal).to eq 20.00
      expect(line_item.pre_tax_subtotal).to eq 20.00
      expect(line_item.tax_amount).to eq 0.00
      expect(line_item.post_tax_subtotal).to eq 20.00
      expect(line_item.total).to eq 20.00
    end

    it 'handles discount' do
      line_item = create(:line_item, quantity: 2, price: 10.00, discount_percent: 25.00)
      expect(line_item.pre_discount_subtotal).to eq 20.00
      expect(line_item.discount_amount).to eq 5.00
      expect(line_item.post_discount_subtotal).to eq 15.00
      expect(line_item.pre_tax_subtotal).to eq 15.00
      expect(line_item.tax_amount).to eq 0.00
      expect(line_item.post_tax_subtotal).to eq 15.00
      expect(line_item.total).to eq 15.00
    end

    it 'handles tax' do
      line_item = create(:line_item, quantity: 2, price: 10.00, tax_percent: 10.00)
      expect(line_item.pre_discount_subtotal).to eq 20.00
      expect(line_item.discount_amount).to eq 0.00
      expect(line_item.post_discount_subtotal).to eq 20.00
      expect(line_item.pre_tax_subtotal).to eq 20.00
      expect(line_item.tax_amount).to eq 2.00
      expect(line_item.post_tax_subtotal).to eq 22.00
      expect(line_item.total).to eq 22.00
    end

    it 'handles discount before tax' do
      line_item = create(:line_item, quantity: 2, price: 10.00, discount_percent: 25.00, tax_percent: 10.00)
      expect(line_item.pre_discount_subtotal).to eq 20.00
      expect(line_item.discount_amount).to eq 5.00
      expect(line_item.post_discount_subtotal).to eq 15.00
      expect(line_item.pre_tax_subtotal).to eq 15.00
      expect(line_item.tax_amount).to eq 1.50
      expect(line_item.post_tax_subtotal).to eq 16.50
      expect(line_item.total).to eq 16.50
    end
  end
end
