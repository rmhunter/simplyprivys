require 'rails_helper'

RSpec.feature 'Admin deletes a rental from a reservation', js: true do
  scenario 'successfully' do
    rental = create(:rental)

    visit reservation_path(rental.reservation, as: create(:admin_user))
    accept_confirm do
      find("#rental_#{rental.id}").click_on 'Delete'
    end

    expect(page).to have_content 'Rental has been deleted'
    expect(rental.reservation.invoice.line_items.length).to eq 0
    expect(rental.reservation.rentals.length).to eq 0
  end
end