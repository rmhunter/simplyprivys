class RemoveLocations < ActiveRecord::Migration[5.1]
  def change
    drop_table :locations do |t|
      t.string :name, null: false
      t.string :address
      t.text :note

      t.timestamps
    end
  end
end
