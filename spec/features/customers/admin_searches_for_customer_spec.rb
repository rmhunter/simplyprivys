require 'rails_helper'

RSpec.feature 'Admin searches for a customer', js: true do
  scenario 'via customer email' do
    customer1 = create(:customer, email: 'JDirt@example.com')
    customer2 = create(:customer)

    visit customers_path(as: create(:admin_user))

    within('#customer-search-form') do
      fill_in 'search_term', with: 'jd'
      find('#search_term').send_keys(:return)
    end

    within('#customers-table') do
      expect(page).to have_content customer1.email
      expect(page).to_not have_content customer2.email
    end
  end

  scenario 'via customer first name' do
    customer1 = create(:customer, first_name: 'Joe')
    customer2 = create(:customer, first_name: 'Steve')

    visit customers_path(as: create(:admin_user))

    within('#customer-search-form') do
      fill_in 'search_term', with: 'J'
      find('#search_term').send_keys(:return)
    end

    within('#customers-table') do
      expect(page).to have_content customer1.first_name
      expect(page).to_not have_content customer2.first_name
    end
  end

  scenario 'via customer last name' do
    customer1 = create(:customer, last_name: 'Dirt')
    customer2 = create(:customer, last_name: 'Buscemi')

    visit customers_path(as: create(:admin_user))

    within('#customer-search-form') do
      fill_in 'search_term', with: 'D'
      find('#search_term').send_keys(:return)
    end

    within('#customers-table') do
      expect(page).to have_content customer1.last_name
      expect(page).to_not have_content customer2.last_name
    end
  end

  scenario 'via customer phone' do
    customer1 = create(:customer, phone: '123-456-7890')
    customer2 = create(:customer, phone: '555-555-5555')

    visit customers_path(as: create(:admin_user))

    within('#customer-search-form') do
      fill_in 'search_term', with: '456'
      find('#search_term').send_keys(:return)
    end

    within('#customers-table') do
      expect(page).to have_content customer1.phone
      expect(page).to_not have_content customer2.phone
    end
  end
end