require 'rails_helper'
require 'support/requests/clearance_helpers'

RSpec.describe "Customers Endpoints", type: :request do

  describe 'GET /customers' do
    it 'responds success with a list of customers' do
      customers = create_list(:customer, 2)

      sign_in_as(create(:admin_user))
      get '/customers'

      expect(response.status).to eq 200
      expect(assigns(:customers)).to include customers.first
      expect(assigns(:customers)).to include customers.last
      expect(response).to render_template :index
    end

    it "filters by first name, last name, email, or phone matching the search term" do
      search_term = 'abc'
      customer_matching_first_name = create(:customer, first_name: search_term)
      customer_matching_last_name = create(:customer, last_name: search_term)
      customer_matching_email = create(:customer, email: search_term)
      customer_matching_phone = create(:customer, phone: search_term)
      customer_without_match = create(:customer)

      sign_in_as(create(:admin_user))
      get '/customers', params: { search_term: search_term }

      expect(assigns(:customers)).to include customer_matching_first_name
      expect(assigns(:customers)).to include customer_matching_last_name
      expect(assigns(:customers)).to include customer_matching_email
      expect(assigns(:customers)).to include customer_matching_phone
      expect(assigns(:customers)).not_to include customer_without_match
    end

    it 'sorts by the given sort param' do
      zedd = create(:customer, first_name: 'Zedd')
      alvin = create(:customer, first_name: 'Alvin')

      sign_in_as(create(:admin_user))
      get '/customers', params: { sort_by: 'first_name', sort_order: 'asc' }

      expect(assigns(:customers).first).to eq alvin
      expect(assigns(:customers).last).to eq zedd
    end

    it 'sorts by the given sort order' do
      alvin = create(:customer, first_name: 'Alvin')
      zedd = create(:customer, first_name: 'Zedd')

      sign_in_as(create(:admin_user))
      get '/customers', params: { sort_by: 'first_name', sort_order: 'desc' }

      expect(assigns(:customers).first).to eq zedd
      expect(assigns(:customers).last).to eq alvin
    end

    it 'requires authenticated user' do
      get '/customers'
      expect_response_to_require_login
    end
  end

  describe 'GET /customers/:id' do
    it 'responds success with the details of the customer' do
      customer = create(:customer)

      sign_in_as(create(:admin_user))
      get "/customers/#{customer.id}"

      expect(response.status).to eq 200
      expect(assigns(:customer)).to eq customer
      expect(response).to render_template :show
    end

    it 'requires authenticated user' do
      get "/customers/#{create(:customer).id}"
      expect_response_to_require_login
    end
  end

  describe 'GET /customers/new' do
    it 'responds with a new customer form' do
      sign_in_as(create(:admin_user))
      get '/customers/new'

      expect(response.status).to eq 200
      expect(assigns(:customer)).to be_a_new Customer
      expect(response).to render_template :new
    end

    it 'requires authenticated user' do
      get '/customers/new'
      expect_response_to_require_login
    end
  end

  describe 'POST /customers' do
    context 'with valid parameters' do
      it 'creates the customer and redirects to the customer' do
        valid_attributes = attributes_for(:customer).compact

        sign_in_as(create(:admin_user))
        post '/customers', params: { customer: valid_attributes }

        customer = Customer.last
        expect(response).to redirect_to customer
        expect(Customer.count).to eq 1
        expect(customer).to have_attributes(valid_attributes)
      end
    end

    it 'requires authenticated user' do
      post '/customers', params: { customer: attributes_for(:customer).compact }
      expect_response_to_require_login
    end
  end

  describe 'GET /customers/:id/edit' do
    it 'responds with a form to edit the given customer' do
      customer = create(:customer)

      sign_in_as(create(:admin_user))
      get "/customers/#{customer.id}/edit"

      expect(response.status).to eq 200
      expect(assigns(:customer)).to eq customer
      expect(response).to render_template :edit
    end

    it 'requires authenticated user' do
      get "/customers/#{create(:customer).id}/edit"
      expect_response_to_require_login
    end
  end

  describe 'PATCH /customers/:id' do
    it 'updates the customer and redirects to the customer' do
      customer = create(:customer)
      new_email = 'new_email@example.com'

      sign_in_as(create(:admin_user))
      patch "/customers/#{customer.id}", params: { customer: { email: new_email } }

      expect(response).to redirect_to customer
      expect(customer.reload.email).to eq new_email
    end

    it 'requires authenticated user' do
      customer = create(:customer)
      patch "/customers/#{customer.id}", params: { customer: { email: 'new_email@example.com' } }
      expect_response_to_require_login
    end
  end

end