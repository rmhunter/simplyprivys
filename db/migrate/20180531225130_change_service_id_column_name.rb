class ChangeServiceIdColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :services, :service_id, :service_type_id
  end
end
