class AddEventTypeToReservations < ActiveRecord::Migration[5.1]
  def change
    add_column :reservations, :event_type, :string
  end
end
