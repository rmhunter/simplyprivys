require 'rails_helper'

RSpec.describe ReservationForm, type: :model do
  describe "validations" do
  end

  describe "#initialize" do
    it 'sets the customer if passed in as a param' do
      customer = create(:customer)
      reservation_form = ReservationForm.new(customer_id: customer.id)
      expect(reservation_form.customer).to eq customer
    end
  end

  describe '#save' do
    it 'saves the reservation' do
      reservation_form = build(:reservation_form)

      reservation_form.save

      reservation = Reservation.last
      expect(Reservation.count).to eq 1
      expect(reservation.customer).to eq reservation_form.customer
    end

    it 'creates a customer if in-line fields were used' do
      reservation_form = build(:reservation_form, :with_new_customer)

      reservation_form.save

      customer = Customer.last
      expect(Customer.count).to eq 1
      expect(customer.first_name).to eq reservation_form.customer_first_name
      expect(customer.last_name).to eq reservation_form.customer_last_name
      expect(customer.email).to eq reservation_form.customer_email
      expect(customer.phone).to eq reservation_form.customer_phone
    end
  end
end
