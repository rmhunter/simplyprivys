class InquiriesController < ApplicationController
  def create
    @inquiry = Inquiry.new(inquiry_params)
    respond_to do |format|
      if @inquiry.save
        format.html { redirect_to root_path, notice: "Inquiry submitted successfully" }
        format.json { render json: @inquiry }
      else
        format.html { render 'visitors/index' }
        format.json { render json: @inquiry.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def inquiry_params
    params.require(:inquiry).permit(:first_name, :last_name, :email, :phone,
                                    :company_name, :event_date, :event_type,
                                    :event_location, :number_of_guests,
                                    :additional_notes)
  end
end
