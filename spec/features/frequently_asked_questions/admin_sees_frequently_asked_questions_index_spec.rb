require 'rails_helper'

RSpec.describe 'Admin sees frequently asked questions index page' do
  scenario 'successfully' do
    faq = create_list(:frequently_asked_question, 2)

    visit admin_frequently_asked_questions_path(as: create(:admin_user))

    expect(page).to have_content(faq.first.question)
    expect(page).to have_content(faq.second.question)
  end
end