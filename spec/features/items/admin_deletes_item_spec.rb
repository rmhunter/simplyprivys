require 'rails_helper'

RSpec.feature "Admin deletes an item", js: true do
  scenario "successfully" do
    item = create(:item)

    visit admin_items_path(as: create(:admin_user))
    accept_confirm do
      delete_button(item).click
    end

    expect(page).to have_content 'Item was successfully destroyed'
  end

  def delete_button(item)
    within "tr[data-url='#{admin_item_path(item)}']" do
      find('[data-method=delete]')
    end
  end
end