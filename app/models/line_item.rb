class LineItem < ApplicationRecord
  belongs_to :itemizable, polymorphic: true, optional: true
  belongs_to :invoice, touch: true
  validates :invoice, presence: true
  validates :price, presence: true, numericality: true
  validates :quantity, presence: true, numericality: true
  validates :discount_percent, presence: true,
            numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }
  validates :tax_percent, presence: true,
            numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }

  def pre_discount_subtotal
    (quantity * price).round(2)
  end

  def discount_amount
    (pre_discount_subtotal * (discount_percent / 100)).round(2)
  end

  def post_discount_subtotal
    pre_discount_subtotal - discount_amount
  end

  def pre_tax_subtotal
    post_discount_subtotal
  end

  def tax_amount
    (pre_tax_subtotal * (tax_percent / 100)).round(2)
  end

  def post_tax_subtotal
    pre_tax_subtotal + tax_amount
  end

  def total
    post_tax_subtotal
  end
end
