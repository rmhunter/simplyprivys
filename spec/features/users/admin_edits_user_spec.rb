require 'rails_helper'
require "support/features/clearance_helpers"

RSpec.feature "Admin edits a user" do
  scenario "successfully from index" do
    user = create(:user)

    visit admin_users_path(as: create(:admin_user))
    edit_button(user).click
    fill_in 'user[email]', with: 'newemail@example.com'
    check 'user[admin]'
    click_on 'Save User'

    expect(page).to have_content 'User was successfully updated'
    expect(user.reload.email).to eq 'newemail@example.com'
    expect(user.reload.admin?).to be_truthy
  end

  scenario "successfully from show" do
    user = create(:user)

    visit admin_users_path(as: create(:admin_user))
    click_on user.id
    edit_button(user).click
    fill_in 'user[email]', with: 'newemail@example.com'
    check 'user[admin]'
    click_on 'Save User'

    expect(page).to have_content 'User was successfully updated'
    expect(user.reload.email).to eq 'newemail@example.com'
    expect(user.reload.admin?).to be_truthy
  end

  def edit_button(user)
    find("a[href='#{edit_admin_user_path(user)}']")
  end
end