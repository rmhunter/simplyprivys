class LineItemsController < ApplicationController
  def new
    @line_item = LineItem.new
    if params[:rental_id]
      rental = Rental.find(params[:rental_id])
      @line_item.itemizable = rental
      ItemizableLineItemBuilder.new(@line_item).run
    end
    if params[:service_id]
      service = Service.find(params[:service_id])
      @line_item.itemizable = service
      ItemizableLineItemBuilder.new(@line_item).run
    end
  end

  def create
    @line_item = LineItem.new(line_item_params)
    if @line_item.save
      redirect_to @line_item.invoice.reservation, notice: 'Item saved successfully'
    else
      render :new
    end
  end

  def edit
    @line_item = LineItem.find(params[:id])
  end

  def update
    @line_item = LineItem.find(params[:id])
    if @line_item.update(line_item_params)
      redirect_to @line_item.invoice.reservation, notice: 'Item saved successfully'
    else
      render :edit
    end
  end

  private

  def line_item_params
    params.require(:line_item).permit(:description, :quantity, :price,
                                      :discount_percent, :tax_percent,
                                      :invoice_id, :itemizable_type, :itemizable_id)
  end
end
