require 'rails_helper'
include ActionView::Helpers::NumberHelper

RSpec.describe ContractTerm, type: :model do
  describe "validations" do
    it { should validate_presence_of(:term) }
  end

  describe 'insert_reservation_data_into_contract_term' do
    it 'returns the term after replacing special symbols with data from the reservation' do
      contract_term1 = create(:contract_term, term: '{{invoice_total}}')
      contract_term2 = create(:contract_term, term: '{{security_deposit}}')
      reservation = create(:reservation)
      line_item = create(:line_item)
      reservation.invoice.line_items = [line_item]

      expect(contract_term1.insert_reservation_data_into_contract_term(reservation)).to eq(number_to_currency(reservation.invoice.total))
      expect(contract_term2.insert_reservation_data_into_contract_term(reservation)).to eq(number_to_currency((reservation.invoice.total * 0.20).round(2)))
    end
  end
end
