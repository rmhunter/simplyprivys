class TrailerInteriorFeature < ApplicationRecord
  validates :feature, presence: true
  default_scope { order :position }
end
