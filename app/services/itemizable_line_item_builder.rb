class ItemizableLineItemBuilder

  def initialize(line_item)
    @line_item = line_item
  end

  def run
    line_item.invoice = @line_item.itemizable.reservation.invoice
    line_item.description = @line_item.itemizable.billing_description
    line_item.quantity = @line_item.itemizable.billing_quantity
    line_item.price = @line_item.itemizable.billing_price
    line_item.tax_percent = @line_item.itemizable.billing_tax_rate
  end

  private

  attr_reader :line_item
end
