global.$ = require('jquery');

$(document).ready(function() {

    $hero = $('#hero.bg-fade');

    if ($hero.length) {
        console.log("Hero found, starting photo fade");

        const IMAGE_URLS = [
            'hero/wedding_lighting.jpg',
            'hero/farmers_market.jpg',
            'hero/wedding_field.jpg',
            'hero/running_race.jpg',
            'hero/construction.jpg',
        ];

        const slidesCount = IMAGE_URLS.length;
        var currentSlide = 0;

        var nextSlide = function() {
            if (currentSlide > slidesCount - 1) {
                currentSlide = 0;
            }
            const url = IMAGE_URLS[currentSlide];
            $hero.css("background-image", 'url("/assets/' + url + '")').show(0, function() {
                setTimeout(nextSlide, 5000);
            });
            currentSlide++;
        }

        nextSlide();
    }

});
