require 'rails_helper'

RSpec.feature "Admin sees a customer" do
  scenario "successfully" do
    customer = create(:customer)

    visit customers_path(as: create(:admin_user))
    click_on customer.email

    expect(current_path).to eq customer_path(customer)
    expect(page).to have_content customer.email
  end
end