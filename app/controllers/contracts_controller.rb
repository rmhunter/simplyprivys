class ContractsController < ApplicationController
  layout 'printable/application'
  before_action :require_login
  before_action :require_admin

  def show
    @reservation = Reservation.find(params[:reservation_id])
    @contract_terms = ContractTerm.all
  end
end