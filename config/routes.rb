Rails.application.routes.draw do
  constraints Clearance::Constraints::SignedIn.new { |user| user.admin? } do
    root to: redirect('/dashboard'), as: :admin_root
  end

  root to: redirect('/restroom-rentals')
  get '/restroom-rentals', to: 'visitors#index', as: :landing_page
  get '/restroom-rentals/:slug', to: 'visitors#index'
  get '/sitemap.:format.:compression', to: 'sitemaps#show'

  resource :dashboard, only: [:show]

  resource :visitor_page_content, only: [:edit, :create]

  resources :inquiries, only: [:create]

  resources :reservations, shallow: true do
    resources :rentals, only: [:new]
    resources :services, only: [:new]
    resources :invoices, only: [:new]
    resource :contract, only: [:show]
  end

  resources :customers, shallow: true do
    resources :reservations, only: [:new]
  end

  resources :services, only: [:create, :edit, :update, :destroy], shallow: true do
    resource :line_item, only: [:new]
  end

  resources :rentals, only: [:create, :edit, :update, :destroy], shallow: true do
    resource :line_item, only: [:new]
  end

  resources :line_items, only: [:create, :edit, :update]

  resources :invoices

  namespace :admin do
    resources :items
    resources :service_types
    resources :seo_locations
    resources :locations
    resources :contract_terms do
      collection do
        patch :sort
      end
    end
    resources :frequently_asked_questions do
      collection do
        patch :sort
      end
    end
    resources :trailer_interior_features do
      collection do
        patch :sort
      end
    end
    resources :trailer_optional_features do
      collection do
        patch :sort
      end
    end
    resources :users, only: [:index, :show, :edit, :update, :destroy]
    root to: "reservations#index"
  end

  ##
  # Clearance (Authentication) Routes
  #
  resources :passwords, controller: 'clearance/passwords', only: [:create, :new]
  resource :session, controller: 'clearance/sessions', only: [:create]
  resources :users, controller: 'clearance/users', only: [:create] do
    resource :password,
             controller: 'clearance/passwords',
             only: [:create, :edit, :update]
  end
  get '/sign_in' => 'clearance/sessions#new', as: 'sign_in'
  delete '/sign_out' => 'clearance/sessions#destroy', as: 'sign_out'
  get '/sign_up' => 'clearance/users#new', as: 'sign_up'
end