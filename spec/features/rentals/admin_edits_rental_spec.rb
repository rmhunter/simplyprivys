require 'rails_helper'

RSpec.feature 'Admin edits a rental on a reservation' do
  scenario 'sees form to edit the rental' do
    reservation = create(:reservation)
    rental = create(:rental, reservation: reservation)

    visit reservation_path(reservation, as: create(:admin_user))
    within "#rental_#{rental.id}" do
      click_on 'Edit'
    end

    expect(current_path).to eq edit_rental_path(rental)
    expect(page).to have_content('Item')
    expect(page).to have_content('Duration type')
    expect(page).to have_content('Start time')
    expect(page).to have_content('End time')
  end

  scenario 'sees line item form after saving rental' do
    reservation = create(:reservation)
    rental = create(:rental, reservation: reservation)
    line_item = create(:line_item, itemizable: rental, invoice: reservation.invoice)

    visit reservation_path(reservation, as: create(:admin_user))
    within "#rental_#{rental.id}" do
      click_on 'Edit'
    end
    click_on 'Continue'

    expect(current_path).to eq edit_line_item_path(line_item)
  end

  scenario 'successfully edits rental' do
    reservation = create(:reservation)
    rental = create(:rental, reservation: reservation)
    new_item = create(:item, name: 'New Item')

    visit reservation_path(reservation, as: create(:admin_user))
    within "#rental_#{rental.id}" do
      click_on 'Edit'
    end
    select new_item.name, from: 'rental_item_id'
    click_on 'Continue'

    expect(rental.reload.item).to eq new_item
  end

  scenario 'successfully edits rental line item' do
    reservation = create(:reservation)
    rental = create(:rental, reservation: reservation)
    line_item = create(:line_item, itemizable: rental, invoice: reservation.invoice)

    visit reservation_path(reservation, as: create(:admin_user))
    within "#rental_#{rental.id}" do
      click_on 'Edit'
    end
    click_on 'Continue'
    # now we're on the line item edit page
    fill_in 'line_item_description', with: 'New Description'
    click_on 'Save'

    expect(page).to have_content 'New Description'
    expect(line_item.reload.description).to eq 'New Description'
  end
end
