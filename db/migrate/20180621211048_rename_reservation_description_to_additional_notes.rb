class RenameReservationDescriptionToAdditionalNotes < ActiveRecord::Migration[5.1]
  def change
    rename_column :reservations, :description, :additional_notes
  end
end
