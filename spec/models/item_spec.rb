require 'rails_helper'

RSpec.describe Item, type: :model do
  describe 'validations' do
    it { should validate_presence_of :name }
    it { should validate_presence_of :price_per_day }
    it { should validate_numericality_of :price_per_day }
  end

  describe 'associations' do
    it { should have_many :rentals }
  end
end
