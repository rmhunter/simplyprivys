/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

// Stylesheet Dependencies
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome/styles.css";
import "flatpickr/dist/flatpickr.min.css"

// Custom Styles
import "../shared/global.css";
import "../visitors/visitors.css";
import "../application/flatpickr.css";

// JavaScript Dependencies
import "jquery";
import "popper.js/dist/popper.min";
import "bootstrap/dist/js/bootstrap.min";
import "@fortawesome/fontawesome/index";
import "@fortawesome/fontawesome-free-regular/index";
import "@fortawesome/fontawesome-free-solid/index";
import "@fortawesome/fontawesome-free-brands/index";
import "moment";
import "flatpickr";


// Custom Javascript
import "../visitors/hero_bg_fade";
import "../visitors/smooth_scroll";
import "../visitors/inquiry_ajax_form";

console.log('Hello World from `visitors` pack via Webpacker');
