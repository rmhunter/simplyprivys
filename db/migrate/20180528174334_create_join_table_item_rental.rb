class CreateJoinTableItemRental < ActiveRecord::Migration[5.1]
  def change
    create_join_table :items, :rentals do |t|
      t.index [:item_id, :rental_id]
    end
  end
end
