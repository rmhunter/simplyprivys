require 'rails_helper'

RSpec.feature "Admin edits a item" do
  scenario "successfully from index" do
    item = create(:item)

    visit admin_items_path(as: create(:admin_user))
    edit_button(item).click
    fill_in 'item[name]', with: 'New Item Name'
    click_on 'Save Item'

    expect(page).to have_content 'Item was successfully updated'
    expect(Item.last.name).to eq 'New Item Name'
  end

  scenario "successfully from show" do
    item = create(:item)

    visit admin_items_path(as: create(:admin_user))
    click_on item.name
    edit_button(item).click
    fill_in 'item[name]', with: 'New Item Name'
    click_on 'Save Item'

    expect(page).to have_content 'Item was successfully updated'
    expect(Item.last.name).to eq 'New Item Name'
  end

  def edit_button(item)
    find("a[href='#{edit_admin_item_path(item)}']")
  end
end