class Service < ApplicationRecord
  belongs_to :reservation
  belongs_to :service_type
  has_one :line_item, as: :itemizable, dependent: :destroy
  validates :name, presence: true
  before_validation :fallback_to_service_type_values

  def billing_description
    name
  end

  def billing_quantity
    1
  end

  def billing_price
    price
  end

  def billing_tax_rate
    8.45
  end

  def to_s
    billing_description
  end

  private

  def fallback_to_service_type_values
    if service_type.present?
      self.name ||= service_type.name
      self.price = service_type.price if (self.price.nil? || self.price == 0)
      self.description ||= service_type.description
    end
  end
end
