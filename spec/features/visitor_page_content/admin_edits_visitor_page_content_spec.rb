require 'rails_helper'

RSpec.describe 'Admin edits visitor page content' do
  scenario 'successfully' do
    create(:singular_page_content, section: 'phone', content: '123')
    create(:singular_page_content, section: 'email', content: 'email@example.com')
    create(:singular_page_content, section: 'website_header', content: 'Website header')
    create(:singular_page_content, section: 'website_subheader', content: 'Website subheader')
    create(:singular_page_content, section: 'how_it_works_header', content: 'How it works header')
    create(:singular_page_content, section: 'contact_header', content: 'Contact header')

    visit edit_visitor_page_content_path(as: create(:admin_user))

    fill_in 'visitor_page_content_phone', with: '555-555-5555'
    fill_in 'visitor_page_content_email', with: 'newemail@example.com'
    fill_in 'visitor_page_content_website_header', with: 'New Website Header'
    fill_in 'visitor_page_content_website_subheader', with: 'New Website Subheader'
    fill_in 'visitor_page_content_how_it_works_header', with: 'New how it works header'
    fill_in 'visitor_page_content_contact_header', with: 'New contact header'
    click_on 'Save'

    phone_scp = SingularPageContent.find_by(section: 'phone')
    email_scp = SingularPageContent.find_by(section: 'email')
    website_header_scp = SingularPageContent.find_by(section: 'website_header')
    website_subheader_scp = SingularPageContent.find_by(section: 'website_subheader')
    website_how_it_works_header = SingularPageContent.find_by(section: 'how_it_works_header')
    website_contact_header_scp = SingularPageContent.find_by(section: 'contact_header')
    expect(page).to have_content 'Content has been updated'
    expect(phone_scp.content).to eq '555-555-5555'
    expect(email_scp.content).to eq 'newemail@example.com'
    expect(website_header_scp.content).to eq 'New Website Header'
    expect(website_subheader_scp.content).to eq 'New Website Subheader'
    expect(website_how_it_works_header.content).to eq 'New how it works header'
    expect(website_contact_header_scp.content).to eq 'New contact header'
  end
end
