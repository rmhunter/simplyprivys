class Dashboard

  def reservations_today
    @reservations_today ||= Reservation.where(event_date: Time.current.to_date)
  end

  def reservations_upcoming
    @reservations_upcoming ||= Reservation.where('event_date >= ?', Date.tomorrow).order('event_date ASC')
  end

  def reservations_inquiring
    @reservations_inquiring ||= Reservation.inquired.order('created_at DESC')
  end
end