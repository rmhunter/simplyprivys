require 'rails_helper'

RSpec.describe VisitorPageContent, type: :model do
  describe "validations" do
    it { should validate_presence_of(:phone) }
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:website_header) }
    it { should validate_presence_of(:website_subheader) }
    it { should validate_presence_of(:how_it_works_header) }
    it { should validate_presence_of(:contact_header) }
  end

  describe '#initialize' do
    it 'loads the existing content values' do
      create(:singular_page_content, section: 'phone', content: '123')

      vcp = VisitorPageContent.new

      expect(vcp.phone).to eq '123'
    end
  end

  describe '#get_singular_content' do
    it 'loads the content value for a given section' do
      create(:singular_page_content, section: 'phone', content: '123')

      vcp = VisitorPageContent.new

      phone_scp = vcp.get_singular_content(:phone)

      expect(phone_scp).to eq '123'
    end
  end

  describe '#save' do
    it 'persists the phone content as a SingularPageContent' do
      vcp = build(:visitor_page_content, phone: '555-555-5555')

      vcp.save

      phone_scp = SingularPageContent.find_by(section: 'phone')
      expect(phone_scp.content).to eq '555-555-5555'
    end
  end
end
