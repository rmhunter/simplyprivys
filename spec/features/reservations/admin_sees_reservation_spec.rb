require 'rails_helper'

RSpec.feature "Admin sees a customer" do
  scenario "successfully" do
    reservation = create(:reservation)

    visit reservations_path(as: create(:admin_user))
    click_on reservation.name

    expect(page).to have_content reservation.name
  end
end