require "rails_helper"

RSpec.feature "Admin sees SEO Locations index page" do
  scenario "successfully" do
    seo_locations = create_list(:seo_location, 2)

    visit admin_seo_locations_path(as: create(:admin_user))

    expect(page).to have_content(seo_locations.first.name)
    expect(page).to have_content(seo_locations.second.name)
  end
end