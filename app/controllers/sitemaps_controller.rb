require 'open-uri'

class SitemapsController < ApplicationController
  def show
    bucket_name = 'simply-privys-production'
    sitemap_s3_url = "https://s3-us-west-2.amazonaws.com/#{bucket_name}/sitemaps/sitemap.xml.gz"
    data = open(sitemap_s3_url)
    send_data data.read, type: data.content_type
    # send_file sitemap_s3_url
  end
end
