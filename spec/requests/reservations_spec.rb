require 'rails_helper'
require 'support/requests/clearance_helpers'

RSpec.describe "Reservations Endpoints", type: :request do

  describe 'GET /reservations' do
    it 'responds success with a list of reservations' do
      reservations = create_list(:reservation, 2)

      sign_in_as(create(:admin_user))
      get '/reservations'

      expect(response.status).to eq 200
      expect(assigns(:reservations)).to include reservations.first
      expect(assigns(:reservations)).to include reservations.last
      expect(response).to render_template :index
    end

    it 'requires authenticated user' do
      get '/reservations'
      expect_response_to_require_login
    end
  end

  describe 'GET /reservations/:id' do
    it 'responds success with the details of the reservation' do
      reservation = create(:reservation)

      sign_in_as(create(:admin_user))
      get "/reservations/#{reservation.id}"

      expect(response.status).to eq 200
      expect(assigns(:reservation)).to eq reservation
      expect(response).to render_template :show
    end

    it 'requires authenticated user' do
      get '/reservations'
      expect_response_to_require_login
    end
  end

  describe 'GET reservations/new' do
    it 'responds with a new reservation form' do
      sign_in_as(create(:admin_user))
      get '/reservations/new'

      expect(response.status).to eq 200
      expect(assigns(:reservation_form)).to be_a_new ReservationForm
      expect(response).to render_template :new
    end

    it 'requires authenticated user' do
      get '/reservations'
      expect_response_to_require_login
    end
  end

  describe 'GET /customers/:id/reservations/new' do
    it 'responds with a new reservation form with a customer passed in' do
      customer = create(:customer)

      sign_in_as(create(:admin_user))
      get "/customers/#{customer.id}/reservations/new"

      expect(response.status).to eq 200
      expect(assigns(:reservation_form)).to be_a_new ReservationForm
      expect(response).to render_template :new
    end
  end
  # Need to figure out how to get valid params with nested params
  describe 'POST /reservations' do
    context 'with valid parameters' do
      it 'creates the reservation and redirects to the reservation' do
        valid_attributes = build(:reservation_form).attributes.compact

        sign_in_as(create(:admin_user))
        post '/reservations', params: { reservation_form: valid_attributes }

        reservation = Reservation.last
        expect(response).to redirect_to reservation
        expect(Reservation.count).to eq 1
        customer = Customer.last
        expect(reservation).to have_attributes(customer_id: customer.id)
      end
    end

    it 'requires authenticated user' do
      post '/customers', params: { customer: attributes_for(:customer).compact }
      expect_response_to_require_login
    end
  end

  describe 'GET /reservations/:id/edit' do
    it 'responds with a form to edit the given reservation' do
      reservation = create(:reservation)

      sign_in_as(create(:admin_user))
      get "/reservations/#{reservation.id}/edit"

      expect(status).to eq 200
      expect(assigns(:reservation)).to eq reservation
      expect(response).to render_template :edit
    end
  end

  describe 'PATCH /reservations/:id' do
    it 'updates the reservation and redirects to the reservation' do
      reservation = create(:reservation)
      new_reservation_name = 'New reservation name'

      sign_in_as(create(:admin_user))
      patch "/reservations/#{reservation.id}", params: { reservation: {name: new_reservation_name} }

      expect(response).to redirect_to reservation
      expect(reservation.reload.name).to eq new_reservation_name
    end

    it 'requires authenticated user' do
      get '/reservations'
      expect_response_to_require_login
    end
  end
end