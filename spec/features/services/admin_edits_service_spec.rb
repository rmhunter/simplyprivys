require 'rails_helper'

RSpec.feature 'Admin edits a service on a reservation', js: true do
  scenario 'sees form to edit the service' do
    reservation = create(:reservation)
    service = create(:service, reservation: reservation)

    visit reservation_path(reservation, as: create(:admin_user))
    within "#service_#{service.id}" do
      click_on 'Edit'
    end

    expect(current_path).to eq edit_service_path(service)
    expect(page).to have_content('Service type')
  end

  scenario 'sees line item form after saving service' do
    reservation = create(:reservation)
    service = create(:service, reservation: reservation)
    line_item = create(:line_item, itemizable: service, invoice: reservation.invoice)

    visit reservation_path(reservation, as: create(:admin_user))
    within "#service_#{service.id}" do
      click_on 'Edit'
    end
    click_on 'Continue'

    expect(current_path).to eq edit_line_item_path(line_item)
  end

  scenario 'successfully edits service' do
    reservation = create(:reservation)
    service = create(:service, reservation: reservation)
    new_service_type = create(:service_type, name: 'New Service Type')

    visit reservation_path(reservation, as: create(:admin_user))
    within "#service_#{service.id}" do
      click_on 'Edit'
    end
    select new_service_type.name, from: 'service_service_type_id'
    click_on 'Continue'

    expect(service.reload.service_type).to eq new_service_type
  end

  scenario 'successfully edits service line item' do
    reservation = create(:reservation)
    service = create(:service, reservation: reservation)
    line_item = create(:line_item, itemizable: service, invoice: reservation.invoice)

    visit reservation_path(reservation, as: create(:admin_user))
    within "#service_#{service.id}" do
      click_on 'Edit'
    end
    click_on 'Continue'
    # now we're on the line item edit page
    fill_in 'line_item_description', with: 'New Description'
    click_on 'Save'

    expect(page).to have_content 'New Description'
    expect(line_item.reload.description).to eq 'New Description'
  end
end
