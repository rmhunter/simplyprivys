require 'rails_helper'
require 'support/requests/clearance_helpers'

RSpec.describe "Services Requests", type: :request do

  describe 'POST /services' do
    context 'with valid parameters' do
      it 'creates the service and redirects back to the reservation' do
        valid_attributes = build(:service).attributes.compact

        sign_in_as(create(:admin_user))
        post '/services', params: { service: valid_attributes }

        service = Service.last
        expect(response).to redirect_to new_service_line_item_path(service)
        expect(Service.count).to eq 1
        expect(service).to have_attributes(valid_attributes)
      end
    end

    it 'requires authenticated user' do
      valid_attributes = build(:service).attributes.compact
      post '/services', params: { service: valid_attributes }
      expect_response_to_require_login
    end
  end

  describe 'GET /services/:id/edit' do
    it 'responds with a form to edit the given service' do
      service = create(:service)

      sign_in_as(create(:admin_user))
      get "/services/#{service.id}/edit"

      expect(response.status).to eq 200
      expect(assigns(:service)).to eq service
      expect(response).to render_template :edit
    end

    it 'requires authenticated user' do
      get "/services/#{create(:service).id}/edit"
      expect_response_to_require_login
    end
  end

  describe 'PATCH /services/:id' do
    it 'updates the service and redirects to the parent reservation' do
      pending 'refactor this to match new workflow'
      service = create(:service)
      new_name = 'New Service Name'

      sign_in_as(create(:admin_user))
      patch "/services/#{service.id}", params: { service: {name: new_name} }

      expect(response).to redirect_to service.reservation
      expect(service.reload.name).to eq new_name
    end

    it 'requires authenticated user' do
      service = create(:service)
      patch "/services/#{service.id}", params: { service: {name: 'New Service Name'} }
      expect_response_to_require_login
    end
  end

end