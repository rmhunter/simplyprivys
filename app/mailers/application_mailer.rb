class ApplicationMailer < ActionMailer::Base
  default from: ENV['default_from_email']
  layout 'mailer'
end
