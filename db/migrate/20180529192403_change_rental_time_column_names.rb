class ChangeRentalTimeColumnNames < ActiveRecord::Migration[5.1]
  def change
    rename_column :rentals, :delivery_time, :start_time
    rename_column :rentals, :pickup_time, :end_time
  end
end
