class VisitorPageContentsController < ApplicationController
  before_action :require_login
  before_action :require_admin

  def edit
    @visitor_page_content = VisitorPageContent.new
  end

  def create
    @visitor_page_content = VisitorPageContent.new(visitor_page_content_form_params)
    if @visitor_page_content.save
      flash[:notice] = 'Content has been updated'
      redirect_to edit_visitor_page_content_path
    else
      flash[:danger] = 'Content has not been updated'
      render :edit
    end
  end

  private

  def visitor_page_content_form_params
    params.require(:visitor_page_content).permit(*VisitorPageContent.form_attributes)
  end
end
