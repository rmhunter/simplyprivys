class Reservation < ApplicationRecord
  enum status: { created: 0, inquired: 1, quoted: 2, confirmed: 3,
                 active: 4, completed: 5, cancelled: 6 }

  belongs_to :customer
  has_many :rentals, dependent: :destroy
  has_many :services, dependent: :destroy
  has_one :invoice, dependent: :destroy

  validates :name, presence: true
  validates :customer, presence: true

  after_commit :ensure_invoice_exists

  default_scope { order(created_at: :desc) }

  # allow soft-deleted customers to still appear on a Reservation
  def customer
    Customer.unscoped { super }
  end

  def first_delivery_time
    rentals.present? ? rentals.order(start_time: :asc).first.start_time : nil
  end

  def last_pickup_time
    rentals.present? ? rentals.order(end_time: :desc).first.end_time : nil
  end

  private

  def ensure_invoice_exists
    Invoice.create(reservation: self) unless invoice.present?
  end
end
