require 'rails_helper'

RSpec.describe Service, type: :model do
  describe 'validations' do
    it { should validate_presence_of :name }
  end

  describe 'associations' do
    it { should belong_to :reservation }
    it { should belong_to :service_type }
  end

  describe '#fallback_to_service_type_values' do
    it 'falls back to name, price, and description from the associated service type' do
      service_type = create(:service_type, name: 'ServiceType', price: 1.00, description: 'Description')
      reservation = create(:reservation)

      service = create(:service, service_type: service_type, reservation: reservation,
                                   name: nil, description: nil, price: nil)
      service.save

      expect(service.persisted?).to be_truthy
      expect(service.name).to eq 'ServiceType'
      expect(service.description).to eq 'Description'
      expect(service.price).to eq 1.0
    end
  end

  describe '#billing_description' do
    it 'returns an invoice-friendly description of the service' do
      service = build(:service)
      expect(service.billing_description).to eq "#{service.name}"
    end
  end

  describe '#billing_quantity' do
    it 'defaults to 1' do
      service = build(:service)
      expect(service.billing_quantity).to eq 1
    end
  end

  describe '#billing_price' do
    it 'defaults to the price of the service' do
      service = build(:service)
      expect(service.billing_price).to eq service.price
    end
  end
end
