require 'rails_helper'

RSpec.feature "Admin deletes reservation", js: true do
  scenario "Successfully" do
    reservation = create(:reservation)

    visit reservations_path(as: create(:admin_user))
    save_screenshot('delete_res.png')
    accept_confirm do
      within "#reservation_#{reservation.id}" do
        click_on 'Delete'
      end
    end

    expect(page).to have_content 'Reservation has been deleted'
  end
end
