require 'rails_helper'

RSpec.feature 'Admin creates a reservation' do
  scenario 'with existing customer' do
    customer = create(:customer, :with_all_fields)

    visit reservations_path(as: create(:admin_user))
    click_on('New Reservation')
    select customer, from: 'reservation_form_customer_id'
    click_on 'Save Reservation'

    expect(page).to have_content 'Reservation has been created'
    reservation = Reservation.last
    expect(reservation.customer).to eq customer
  end

  scenario 'and creates a new customer in-line' do
    visit reservations_path(as: create(:admin_user))
    click_on('New Reservation')
    fill_in 'reservation_form_customer_first_name', with: 'Bob'
    click_on 'Save Reservation'

    expect(page).to have_content 'Reservation has been created'
    reservation = Reservation.last
    customer = Customer.last
    expect(reservation.customer).to eq customer
    expect(customer.first_name).to eq 'Bob'
  end

  scenario 'via customer' do
    customer = create(:customer, :with_all_fields)

    visit customers_path(as: create(:admin_user))
    click_on customer.email
    click_on('New Reservation')
    click_on 'Save Reservation'

    expect(page).to have_content 'Reservation has been created'
    reservation = Reservation.last
    expect(reservation.customer).to eq customer
  end
end
