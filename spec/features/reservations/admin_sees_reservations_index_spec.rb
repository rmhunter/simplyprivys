require "rails_helper"

RSpec.feature "Admin sees reservations index page" do
  scenario "successfully" do
    reservation = create_list(:reservation, 2)

    visit reservations_path(as: create(:admin_user))

    expect(page).to have_content(reservation.first.name)
    expect(page).to have_content(reservation.second.name)
  end
end