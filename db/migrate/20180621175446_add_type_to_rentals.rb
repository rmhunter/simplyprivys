class AddTypeToRentals < ActiveRecord::Migration[5.1]
  def change
    add_column :rentals, :duration_type, :integer, index: true, null: false, default: 0
  end
end
