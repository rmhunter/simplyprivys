// from https://blackrockdigital.github.io/startbootstrap-simple-sidebar/

global.$ = require('jquery');

$(document).ready(function() {

    $("#sidebar-toggler").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    // show sidebar by default if tablet or larger
    if ($(window).width() >= 992) {
        $("#wrapper").toggleClass("toggled");
    }

    // allow animations after page load
    setTimeout(function() {
        $("#wrapper").removeClass("loading");
    }, 500);

});
