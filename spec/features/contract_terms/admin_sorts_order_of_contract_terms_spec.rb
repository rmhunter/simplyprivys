require 'rails_helper'

RSpec.describe 'Admin sorts contract terms', js: true do
  scenario 'via drag and drop' do
    pending("Fix test with drag and drop")
    contract_term1 = create(:contract_term, term: 'PAYMENT')
    contract_term2 = create(:contract_term, term: 'UNITS')

    visit admin_contract_terms_path(as: create(:admin_user))

    within(".js-table-row", match: :first) do
      expect(page).to have_content 'PAYMENT'
    end
    within("tbody") do
      drag_source.drag_to(drag_target)
    end

    within(".js-table-row", match: :first) do
      expect(page).to have_content 'UNITS'
    end
  end
end

def drag_source
  within('#contract_term_1') do
    find('.sort-handle')
  end
end

def drag_target
  within('#contract_term_2') do
    find('.sort-handle')
  end
end