require "rails_helper"

RSpec.feature "Admin sees items index page" do
  scenario "successfully" do
    items = create_list(:item, 2)

    visit admin_items_path(as: create(:admin_user))

    expect(page).to have_content(items.first.name)
    expect(page).to have_content(items.second.name)
  end
end