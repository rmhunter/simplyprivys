require 'rails_helper'

RSpec.feature "Admin creates an SEO Location" do
  scenario "successfully" do
    visit admin_seo_locations_path(as: create(:admin_user))

    page.find('[data-role="new-seo_location"]').click

    fill_in 'seo_location[name]', with: 'SEO Location 1'
    click_on 'Save Seo Location'

    expect(page).to have_content 'SeoLocation was successfully created'
    expect(page).to have_content 'SEO Location 1'
    expect(SeoLocation.last.name).to eq 'SEO Location 1'
  end
end
