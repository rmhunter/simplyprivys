class CreateLineItems < ActiveRecord::Migration[5.1]
  def change
    create_table :line_items do |t|
      t.references :invoice, foreign_key: true
      t.date :completed_on
      t.string :description
      t.integer :quantity, null: false, default: 1
      t.decimal :price, precision: 16, scale: 2, null: false, default: 0
      t.decimal :discount_percent, precision: 5, scale: 2, null: false, default: 0
      t.decimal :tax_percent, precision: 5, scale: 2, null: false, default: 0

      t.timestamps
    end
  end
end
