require 'rails_helper'

RSpec.feature "Admin sees a service type" do
  scenario "successfully" do
    service_type = create(:service_type)

    visit admin_service_types_path(as: create(:admin_user))
    click_on service_type.name

    expect(page).to have_content service_type.name
  end
end