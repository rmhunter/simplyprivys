class Invoice < ApplicationRecord
  enum status: { created: 0, sent: 1, paid: 2, void: 3 }
  validates :status, presence: true
  belongs_to :reservation, optional: true
  has_many :line_items, dependent: :destroy, inverse_of: :invoice
  accepts_nested_attributes_for :line_items, allow_destroy: true

  default_scope { order(created_at: :desc) }

  def pre_tax_subtotal
    line_items.reduce(0) { |sum, line_item| sum + line_item.pre_tax_subtotal }
  end

  def tax_amount
    line_items.reduce(0) { |sum, line_item| sum + line_item.tax_amount }
  end

  def total
    pre_tax_subtotal + tax_amount
  end
end
