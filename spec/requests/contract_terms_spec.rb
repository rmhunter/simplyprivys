require 'rails_helper'
require 'support/requests/clearance_helpers'

RSpec.describe 'ContractTerms Endpoints', type: :request do

  describe 'GET /admin/contract_terms' do
    it "responds with a list of contract terms" do
      contract_terms = create_list(:contract_term, 2)
      sign_in_as(create(:admin_user))
      get '/admin/contract_terms'

      expect(response.status).to eq 200
      expect(assigns(:resources)).to eq contract_terms
      expect(response).to render_template :index
    end

    describe 'PATCH /admin/contract_terms/sort' do
      it "responds with new contract_term positions" do
        pending("Fix test for contract_terms/sort")
        create_list(:contract_term, 2)
        sign_in_as(create(:admin_user))
        patch '/admin/contract_terms/sort', params: { contract_term: ["2", "1"] }

        expect(response.status).to eq 200
        expect(ContractTerm.first.position).to eq 2
        expect(ContractTerm.last.position).to eq 1
      end
    end

    it 'requires authenticated user' do
      get '/admin/contract_terms'
      expect_response_to_require_login
    end
  end
end
