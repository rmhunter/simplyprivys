class ServiceType < ApplicationRecord
  validates :name, presence: true
end
