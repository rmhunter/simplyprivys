namespace :sp do
  namespace :db do
    desc "Generate default configuration data, i.e. for a new deploy"
    task seed: :environment do

      #=====================================================================
      # Rental Item(s)
      #=====================================================================
        rental_items = [
            {
                name: '2-Stall Trailer',
                price_per_day: 900,
                price_per_week: 1400,
                price_per_month: 2300,
            },
            {
                name: '3-Stall Trailer',
                price_per_day: 1200,
                price_per_week: 1900,
                price_per_month: 2700,
            },
            {
                name: '5-Stall Trailer',
                price_per_day: 1400,
                price_per_week: 2400,
                price_per_month: 3100,
            },
            {
                name: 'Power Generator',
                price_per_day: 50,
            },
            {
                name: 'Water Tank Filling',
                price_per_day: 50,
            },
        ]

        if Item.count > 0
          puts 'Item(s) already found, skipping'.yellow
        else
          print "Generating rental items..."
          rental_items.each { |attrs_hash| Item.create!(attrs_hash) }
          puts "success".green
        end


      #=====================================================================
      # Service Type(s)
      #=====================================================================
        service_types = [
            {
                name: 'Pumping Service (Included)',
                description: 'One free service included per rental',
                price: 0.00,
            },
            {
                name: 'Pumping Service',
                description: 'Additional pumping service',
                price: 175.00,
            },
            {
                name: 'Delivery Fee',
                description: 'Per-mile beyond the included service area',
                price: 2.00,
            },
            {
                name: 'Attendant (hourly)',
                price: 30.00,
            },
        ]

        if ServiceType.count > 0
          puts 'ServiceType(s) already found, skipping'.yellow
        else
          print "Generating service types..."
          service_types.each { |attrs_hash| ServiceType.create!(attrs_hash) }
          puts "success".green
        end


      #=====================================================================
      # Contract Term(s)
      #=====================================================================
        contract_terms = [
            {
                term: 'RENTAL UNITS: Provider agree to rent to Customer portable restroom units (the "Units"), for placement and operation in the location designated by Customer under the terms and conditions as hereinafter stated.',
            },
            {
                term: 'RENTAL PAYMENT AND DEPOSIT: Customer shall pay to Provider the full sum of {{invoice_total}} (inclusive of tax) at time of delivery of the Units to the location.'
            },
            {
                term: 'RENTAL PERIOD: The Rental Period for the delivered Units is as stated above.'
            },
            {
                term: 'SECURITY DEPOSIT: Customer shall make a refundable, non-interest bearing security deposit in the amount of {{security_deposit}}, at the time of executing this Agreement, unless otherwise agreed to by Provider. The Security Deposit shall secure the full and faithful performance by the Customer of all of the terms and conditions of this Agreement. Provider does not have responsibility to save the date and will fill the Rental Period with another customer if the Security Deposit is not made. At the discretion of the Provider, the Security Deposit may be applied against the costs incurred by the Provider in repairing damages to the Units due to carelessness, negligence, accident or abuse on the part of Customer, guests, agents, or invitees. Provider reserves the right to apply all or any portion of the Security Deposit to any unpaid rental of Customer existing at the time of termination of the Rental Period. Such right only accrues to the Provider; Customer may not unilaterally forfeit the Security Deposit as rental for the specified period. At the discretion of Provider, Customer remains responsible for unpaid rental at termination of the Rental Period, regardless of the disposition of the Security Deposit.'
            },
            {
                term: 'LATE CHARGES: Customer agrees to pay a late charge of $200.00 if the Rental Rate is not paid in a timely manner or agreed upon date.'
            },
            {
                term: 'RETURNED CHECKS: Customer agrees to pay a $35.00 charge for any check returned by a bank for insufficient funds, closed account or for any other reason.'
            },
            {
                term: 'UTILITIES: Customer shall be responsible for all charged utility fees such as water and/or power, incurred by the use of the Units. If other utility items are needed for the operation of the Units, such as a generator, the cost will be added to the Rental Rate, and all additional fees and costs shall be invoiced to Customer within five (5) days of the termination of the Rental Period.'
            },
            {
                term: 'WASTE DISPOSAL: Customer shall be responsible for the cost of servicing and waste disposal of each Unit of portable restroom. The Units" storage tanks hold a limited amount of sewage. It is the responsibility of the Customer to ensure that the storage tank does not overflow. If emptying is needed during the Rental Period, prior arrangements must be made with Provider.'
            },
            {
                term: 'GARBAGE CONTAINERS AND SUPPLIES: Provider will provide all initial toilet paper, trash bags and hand towels. The Customer is responsible for restocking and garbage disposal during the Rental Period unless other prior arrangements are made with Provider. All electrical cords, decorations, water hoses, and other similar equipment or amenities, provided to and in the Units by Provider are the property of the Provider. If any equipment/amenities owned by Provider are not returned while under the care of the Customer, the Customer is financially responsible for any replacement cost incurred by Provider.'
            },
            {
                term: 'REPAIRS AND MAINTENANCE: Customer accepts the Units in the present condition, for the uses and purposes described herein. Customer agrees to maintain the Units in clean and good condition and repair, natural wear and tear excepted.'
            },
            {
                term: "USE AND OCCUPANCY: The Units shall be used for portable restroom purposes and for no other purpose. Customer is not to put the Units to any use which is illegal or creates a nuisance. Smoking and burning candles are not permitted in the Units. Customer will use the Units in a careful and proper manner, in accordance with all applicable local and State laws, rules and regulations and any manufacturer's or supplier's instructions or manuals provided to Customer. Customer will not move or alter the Units without prior written consent of the Provider. If movement is needed after the initial setup, Provider must be notified and if possible will move the Units at an additional charge of $75.00 per hour per person for time and labor. Customer agrees to turn off and remove water supply, remove electrical supply, and close and lock all outside doors to the Unit(s), at night, following each day of use."
            },
            {
                term: 'ASSIGNMENT: Customer shall not sublet the Units, nor reassign this Agreement, or any interest therein, without the prior written consent of the Provider. A violation of this covenant shall constitute a breach of this Agreement, in which case Customer shall forfeit the Security Deposit and Rental Rate. Provider shall also have the right to immediately terminate this Agreement and collect the Units prior to the expiration of the Rental Period.'
            },
            {
                term: 'DISCLAIMER OF WARRANTIES; HOLD HARMLESS. Each Unit is provided and accepted "as-is, where-is", without express or implied warranties. Except as expressly provided herein, Provider shall not have any liability for any direct, indirect, consequential or incidental damages arising out of this Agreement or with respect to the Units.'
            },
            {
                term: 'NON-PAYMENT: If the Customer fails to make payments as specified in this Agreement, Provider may pursue all remedies available by law or in equity, including termination of this Agreement without notice and/or repossession of the equipment without legal process.'
            },
            {
                term: 'CANCELLATIONS: If cancellation is made 30 days or more prior to the Rental Period, Provider shall refund 100% of the Rental Rate, minus any costs already incurred by Provider. If cancellation is made 29 -15 days prior to the Rental Period, Provider shall refund 50% of the Rental Rate, minus any costs already incurred by Provider. If cancellation is made 14 days or less prior to the Rental Period, Provider shall not refund and 100% of the Rental Rate is forfeit.'
            },
            {
                term: "DELIVERY, SETUP AND REMOVAL: Prior to delivery, Provider and Customer will coordinate a delivery, setup and removal time that is acceptable to both parties. Provider will have no responsibility or liability for any delay or failure of delivery caused by Customer. If Provider's delivery, setup or removal is delayed by Customer, Provider shall charge an additional $75.00 per hour for standby time."
            },
            {
                term: 'RETURN OF UNITS: Customer is responsible for returning the Units to Provider in good repair, clean condition and in working order. Upon vacancy or termination of this Agreement, Customer agrees to turn off and remove water supply, remove electrical supply, and close and lock all outside doors to the Unit(s), and return all key(s) to Provider.'
            },
            {
                term: 'GOVERNING LAW: This Agreement shall be governed by the laws of the State of Utah. This Note shall be governed under the laws of the State of Utah. Any action pursuant to this Note shall be brought in the courts of the State of Utah in and for Summit County, which courts shall have exclusive jurisdiction.'
            },
            {
                term: "ATTORNEY'S FEES AND COSTS: In the event either Party brings legal proceedings to interpret or enforce the provisions of this Agreement, the prevailing party shall be entitled to recover in such proceedings all costs and expenses incident thereto, including reasonable attorney's fees."
            }
        ]

        if ContractTerm.count > 0
          puts 'ContractTerm(s) already found, skipping'.yellow
        else
          print "Generating contract terms..."
          contract_terms.each { |attrs_hash| ContractTerm.create!(attrs_hash) }
          puts "success".green
        end


      #=====================================================================
      # Singular Page Content(s)
      #=====================================================================
        singular_page_contents = [
            {section: 'phone', content: '(435) 631-2603'},
            {section: 'email', content: 'info@simplyprivys.com'},
            {section: 'website_header', content: "Restroom trailer rentals for all life's events"},
            {section: 'website_subheader', content: "Rent a simply beautiful portable bathroom, perfect for weddings, corporate retreats, special events, and home remodels."},
            {section: 'how_it_works_header', content: "AS SIMPLE AS 1, 2, 3"},
            {section: 'how_it_works_content', content: "Simply Privys is a local, family-owned business serving all of Utah, including Park City, Salt Lake City, Ogden, Moab, and more. We provide our clients with the latest high-end portable restrooms to accommodate any outdoor restroom need."},
            {section: 'contact_header', content: "CONTACT US FOR A FREE QUOTE"},
            {section: 'seo_page_title', content: "Simply Privys | Restroom Trailer Rentals in Park City, Utah"},
            {section: 'seo_page_title_with_seo_location', content: "Simply Privys | Restroom Trailer Rentals in"},
            {section: 'seo_page_description', content: "Restroom trailer rentals serving all of Utah. Rent a simply beautiful portable restroom, perfect for weddings, corporate retreats, home remodels, and special events."},
        ]

        singular_page_contents.each do |attrs|
          SingularPageContent.find_or_initialize_by(section: attrs[:section]) do |spc|
            print "Generating content for section #{attrs[:section].to_s.titleize}..."
            spc.content = attrs[:content]
            spc.save!
            puts "success".green
          end
        end


      #=====================================================================
      # Frequently Asked Question(s)
      #=====================================================================
        frequently_asked_questions = [
            {question: 'Do the trailers require water and electricity?', answer: 'Each Simply Privys unit may be used with water and power from your location, or can be fully-self contained.'},
            {question: 'What is the cost of a Simply Privys Unit?', answer: 'Simply Privys will work with you to get the best price for your event.'},
            {question: 'How many guests can each trailer accommodate?', answer: 'We have multiple trailers in many configurations that can accommodate an event of any size.'},
            {question: 'How does delivery work and how far will Simply Privys travel?', answer: 'Simply Privys will come to you! An additional charge may be necessary for extended distances.'},
            {question: 'Who is responsible for cleaning the Simply Privys restroom trailer?', answer: 'Simply Privys will clean and service your restroom.'},
        ]

        if FrequentlyAskedQuestion.count > 0
          puts 'FrequentlyAskedQuestion(s) already found, skipping'.yellow
        else
          print "Generating frequently asked questions..."
          frequently_asked_questions.each { |attrs_hash| FrequentlyAskedQuestion.create!(attrs_hash) }
          puts "success".green
        end


      #=====================================================================
      # Trailer interior feature(s)
      #=====================================================================
        trailer_interior_features = [
            { feature: 'Flushing toilets'},
            { feature: 'Custom porcelain sink with Vanity'},
            { feature: 'Sound System to play your favorite music'},
            { feature: 'Full Climate control (heating and A/C)'},
            { feature: 'Falcon Water-Free urinals'},
            { feature: 'Moen Single Hand Faucet'},
            { feature: 'Locking Doors'},
            { feature: 'Fully enclosed stalls'},
            { feature: 'Wainscoting'},
            { feature: 'Crown Molding'},
            { feature: 'Laminate flooring'},
        ]

        if TrailerInteriorFeature.count > 0
          puts 'TrailerInteriorFeature(s) already found, skipping'.yellow
        else
          print "Generating trailer interior features..."
          trailer_interior_features.each { |attrs_hash| TrailerInteriorFeature.create!(attrs_hash) }
          puts "success".green
        end


      #=====================================================================
      # Trailer optional feature(s)
      #=====================================================================
        trailer_optional_features = [
            { feature: 'Custom flower bouquets'},
            { feature: 'Linen upgrade'},
            { feature: 'Custom toiletries'},
            { feature: 'Bench seating'},
            { feature: 'Exterior lighting'},
            { feature: 'Landscape package'},
        ]

        if TrailerOptionalFeature.count > 0
          puts 'TrailerOptionalFeature(s) already found, skipping'.yellow
        else
          print "Generating trailer optional features..."
          trailer_optional_features.each { |attrs_hash| TrailerOptionalFeature.create!(attrs_hash) }
          puts "success".green
        end

      # end of task block

    end
  end
end
