require 'rails_helper'

RSpec.feature 'Admin deletes a service from a reservation', js: true do
  scenario 'successfully' do
    service = create(:service)

    visit reservation_path(service.reservation, as: create(:admin_user))
    accept_confirm do
      find("#service_#{service.id}").click_on 'Delete'
    end

    expect(page).to have_content 'Service has been deleted'
    expect(service.reservation.invoice.line_items.length).to eq 0
    expect(service.reservation.services.length).to eq 0
  end
end