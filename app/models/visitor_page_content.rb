class VisitorPageContent
  include ActiveModel::Model

  def self.form_attributes
    [:phone, :email, :website_header, :website_subheader, :how_it_works_header, :how_it_works_content, :contact_header, :seo_page_title, :seo_page_title_with_seo_location, :seo_page_description]
  end
  attr_accessor *self.form_attributes

  validates :phone, presence: true
  validates :email, presence: true
  validates :website_header, presence: true
  validates :website_subheader, presence: true
  validates :how_it_works_header, presence: true
  validates :how_it_works_content, presence: true
  validates :contact_header, presence: true
  validates :seo_page_title, presence: true
  validates :seo_page_title_with_seo_location, presence: true
  validates :seo_page_description, presence: true

  def initialize(attributes = {})
    super
    load_existing_values
  end

  # def persisted?
  #   false
  # end

  def new_record?
    false
  end

  def save
    ActiveRecord::Base.transaction do
      # perform any pre-validation logic here (such as coercing types or assigning associations from controller params)
      if valid?
        persist!
        true
      else
        false
      end
    end
  end
  alias save! save

  private

  def load_existing_values
    @phone ||= SingularPageContent.value_for(:phone)
    @email ||= SingularPageContent.value_for(:email)
    @website_header ||= SingularPageContent.value_for(:website_header)
    @website_subheader ||= SingularPageContent.value_for(:website_subheader)
    @contact_header ||= SingularPageContent.value_for(:contact_header)
    @how_it_works_header ||= SingularPageContent.value_for(:how_it_works_header)
    @how_it_works_content ||= SingularPageContent.value_for(:how_it_works_content)
    @seo_page_title ||= SingularPageContent.value_for(:seo_page_title)
    @seo_page_title_with_seo_location ||= SingularPageContent.value_for(:seo_page_title_with_seo_location)
    @seo_page_description ||= SingularPageContent.value_for(:seo_page_description)
  end

  def persist!
    self.class.form_attributes.each do |attr|
      spc = SingularPageContent.find_or_initialize_by(section: attr.to_s)
      spc.content = self.public_send(attr.to_sym)
      spc.save!
    end
  end
end
