module InvoicesHelper
  def input_statuses_collection_for_select
    Invoice.statuses.each.map { |slug, integer| [slug.titleize, slug] }
  end

  def invoice_status_context(invoice)
    {
        'created' => 'info',
        'sent' => 'primary',
        'paid' => 'success',
        'void' => 'warning',
    }[invoice.status] || 'default'
  end
end