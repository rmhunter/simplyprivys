require "rails_helper"

RSpec.feature "Admin sees customers index page" do
  scenario "successfully" do
    customers = create_list(:customer, 2)

    visit customers_path(as: create(:admin_user))

    expect(page).to have_content(customers.first.email)
    expect(page).to have_content(customers.second.email)
  end
end