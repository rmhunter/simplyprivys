require 'rails_helper'
require 'support/requests/clearance_helpers'

RSpec.describe "Visitors Requests", type: :request do

  describe 'GET /restroom-rentals' do
    it 'responds success' do
      get '/restroom-rentals'
      expect(response.status).to eq 200
    end

    it 'initializes a new Inquiry' do
      get '/restroom-rentals'
      expect(assigns(:inquiry)).to be_a(Inquiry)
    end

    it 'renders the visitors index' do
      get '/restroom-rentals'
      expect(response).to render_template :index
    end
  end

  describe 'GET /restroom-rentals/:slug' do
    it 'renders a landing page for the given SEO location' do
      seo_location = create(:seo_location, name: 'Salt Lake City, Utah')

      get '/restroom-rentals/salt-lake-city-utah'

      expect(response).to be_success
      expect(assigns(:seo_location)).to eq seo_location
      expect(response).to render_template :index
    end

    context 'with invalid slug' do
      context 'as an admin user' do
        it 'redirects to SEO locations menu' do
          sign_in
          get '/restroom-rentals/invalid-slug'
          expect(response).to redirect_to admin_seo_locations_path
        end
      end

      context 'as a visitor' do
        it 'redirects to landing page' do
          get '/restroom-rentals/invalid-slug'
          expect(response).to redirect_to landing_page_path
        end
      end
    end
  end

end