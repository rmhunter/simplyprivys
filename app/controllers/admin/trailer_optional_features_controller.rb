module Admin
  class TrailerOptionalFeaturesController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = TrailerOptionalFeature.
    #     page(params[:page]).
    #     per(10)
    # end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   TrailerOptionalFeature.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information
    def index
      super
      @resources = TrailerOptionalFeature.all
    end

    def sort
      params[:trailer_optional_feature].each_with_index do |id, index|
        TrailerOptionalFeature.where(id: id).update_all(position: index + 1)
      end
      head :ok
    end
  end
end
