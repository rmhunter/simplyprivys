require 'rails_helper'

RSpec.feature "Admin creates a customer" do
  scenario "successfully" do
    visit customers_path(as: create(:admin_user))

    click_on 'New Customer'
    fill_in 'customer[first_name]', with: 'Bob'
    fill_in 'customer[last_name]', with: 'Dob'
    fill_in 'customer[email]', with: 'bob@example.com'
    click_on 'Save Customer'

    expect(page).to have_content 'Customer has been created'
    expect(page).to have_content 'bob@example.com'
    expect(Customer.last.first_name).to eq 'Bob'
    expect(Customer.last.last_name).to eq 'Dob'
    expect(Customer.last.email).to eq 'bob@example.com'
  end
end
