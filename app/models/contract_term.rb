class ContractTerm < ApplicationRecord
  validates :term, presence: true
  default_scope { order :position }

  def insert_reservation_data_into_contract_term(reservation)
    matchers = {'{{invoice_total}}' => helpers.number_to_currency(reservation.invoice.total), '{{security_deposit}}' => helpers.number_to_currency((reservation.invoice.total * 0.20).round(2))}
    term.gsub(/{{invoice_total}}|{{security_deposit}}/) { |match| matchers[match]}
  end

  # Is there a better way to include helpers in the model?
  def helpers
    ActionController::Base.helpers
  end
end
