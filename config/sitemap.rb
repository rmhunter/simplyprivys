require 'aws-sdk'

# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://www.simplyprivys.com"

# When uploading to s3, it still has to write it locally first.
# Since Heroku only allows writing to tmp/, that's what we'll use
SitemapGenerator::Sitemap.public_path = 'tmp/'

# The remote host where your sitemaps will be hosted
#
# NOTE:
#   If you're storing the sitemaps remotely (i.e. S3 bucket), but you have a local path
#   which you want search engines to use (i.e. a controller which downloads it from S3), DO NOT set this. Instead, you'll want to use refresh:no_ping, then manually ping the local path at the end of this file
#   -- see https://github.com/kjvarga/sitemap_generator/issues/173
#
# SitemapGenerator::Sitemap.sitemaps_host = "https://s3-us-west-2.amazonaws.com/simply-privys-production/"

# Namespace the sitemaps in the s3 bucket
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

# Use S3 for storing sitemaps
SitemapGenerator::Sitemap.adapter = SitemapGenerator::AwsSdkAdapter.new(
    ENV['AWS_S3_BUCKET'],
    aws_access_key_id: ENV['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
    aws_region: ENV['AWS_REGION'],
)

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end

  # SEO-friendly root URL
  add '/restroom-rentals', priority: 1, changefreq: 'always', lastmod: Time.current

  # Generate sitemap URL for each SEO Location
  SeoLocation.find_each do |location|
    if location && location.slug.present?
      add "/restroom-rentals/#{location.slug}",
          lastmod: location.updated_at,
          priority: 0.9,
          changefreq: 'daily'
    end
  end
end

# This is a hack, to have search engines ping locally but store the sitemap remotely
# Since adding this line here triggers a ping as part of the refresh, you should call the refresh task with :no_ping
# see settings/comments above, and see https://github.com/kjvarga/sitemap_generator/issues/173
SitemapGenerator::Sitemap.ping_search_engines('https://www.simplyprivys.com/sitemap.xml.gz')
