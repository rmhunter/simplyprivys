require 'rails_helper'

RSpec.feature "Admin deletes a service type", js: true do
  scenario "successfully" do
    service_type = create(:service_type)

    visit admin_service_types_path(as: create(:admin_user))
    accept_confirm do
      delete_button(service_type).click
    end

    expect(page).to have_content 'ServiceType was successfully destroyed'
  end

  def delete_button(service_type)
    within "tr[data-url='#{admin_service_type_path(service_type)}']" do
      find('[data-method=delete]')
    end
  end
end