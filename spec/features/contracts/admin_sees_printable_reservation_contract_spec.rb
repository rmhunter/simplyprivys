require 'rails_helper'

RSpec.describe 'Admin sees a printable reservation contract' do
  scenario 'successfully' do
    reservation = create(:reservation)

    visit reservation_path(reservation, as: create(:admin_user))

    click_on('Print Contract')

    expect(current_path).to eq reservation_contract_path(reservation)
    expect(page).to have_content reservation.customer.full_name
  end
end