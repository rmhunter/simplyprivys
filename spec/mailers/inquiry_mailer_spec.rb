require "rails_helper"

RSpec.describe InquiryMailer, type: :mailer do

  describe "customer receipt" do
    let(:inquiry) { create(:inquiry, :with_all_fields) }
    let(:mail) { InquiryMailer.customer_receipt(inquiry) }

    it "renders the subject" do
      expect(mail.subject).to eq "Your Simply Privys Inquiry"
    end

    it "sends from the correct email" do
      expect(mail.from).to eq [ENV['admin_email']]
    end

    it "renders the greeting" do
      expect(mail.body.encoded).to match "Thank you"
    end
  end

  describe "admin notification" do
    let(:inquiry) { create(:inquiry, :with_all_fields) }
    let(:mail) { InquiryMailer.admin_notification(inquiry) }

    it "renders the subject" do
      expect(mail.subject).to eq "New Inquiry"
    end

    it "sends from the correct email" do
      expect(mail.from).to eq [ENV['admin_email']]
    end

    it "includes the inquiry details" do
      mail_body = mail.body.encoded
      expect(mail_body).to include inquiry.first_name
      expect(mail_body).to include inquiry.last_name
      expect(mail_body).to include inquiry.email
      expect(mail_body).to include inquiry.phone
      expect(mail_body).to include inquiry.event_date.to_formatted_s(:long)
      expect(mail_body).to include inquiry.event_type
      expect(mail_body).to include inquiry.event_location
      expect(mail_body).to include inquiry.number_of_guests
      expect(mail_body).to include inquiry.additional_notes
    end
  end

end
