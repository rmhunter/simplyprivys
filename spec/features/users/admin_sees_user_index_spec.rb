require "rails_helper"

RSpec.feature "Admin sees users index page" do
  scenario "successfully" do
    users = create_list(:user, 2)

    visit admin_users_path(as: create(:admin_user))

    expect(page).to have_content(users.first.email)
    expect(page).to have_content(users.second.email)
  end
end