require 'rails_helper'

RSpec.feature "Admin deleted a customer" do
  scenario "successfully" do
    customer = create(:customer)

    visit customers_path(as: create(:admin_user))
    click_on customer.email
    page.find('.delete-customer-button').click

    expect(page).to have_content 'Customer has been deleted'
  end
end
