# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180717172142) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contract_terms", force: :cascade do |t|
    t.text "term", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "display_order"
    t.integer "position"
  end

  create_table "customers", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "phone"
    t.string "company_name"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_customers_on_deleted_at"
    t.index ["email"], name: "index_customers_on_email"
    t.index ["first_name"], name: "index_customers_on_first_name"
    t.index ["last_name"], name: "index_customers_on_last_name"
    t.index ["phone"], name: "index_customers_on_phone"
  end

  create_table "frequently_asked_questions", force: :cascade do |t|
    t.text "question", null: false
    t.text "answer", null: false
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "friendly_id_slugs", id: :serial, force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "invoices", force: :cascade do |t|
    t.bigint "reservation_id"
    t.date "due_on"
    t.datetime "sent_at"
    t.integer "status", default: 0, null: false
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["due_on"], name: "index_invoices_on_due_on"
    t.index ["reservation_id"], name: "index_invoices_on_reservation_id"
    t.index ["status"], name: "index_invoices_on_status"
  end

  create_table "items", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "price_per_day", precision: 16, scale: 2, default: "0.0", null: false
    t.decimal "price_per_week", precision: 16, scale: 2, default: "0.0", null: false
    t.decimal "price_per_month", precision: 16, scale: 2, default: "0.0", null: false
  end

  create_table "line_items", force: :cascade do |t|
    t.bigint "invoice_id"
    t.date "completed_on"
    t.string "description"
    t.integer "quantity", default: 1, null: false
    t.decimal "price", precision: 16, scale: 2, default: "0.0", null: false
    t.decimal "discount_percent", precision: 5, scale: 2, default: "0.0", null: false
    t.decimal "tax_percent", precision: 5, scale: 2, default: "0.0", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "itemizable_type"
    t.bigint "itemizable_id"
    t.index ["invoice_id"], name: "index_line_items_on_invoice_id"
    t.index ["itemizable_type", "itemizable_id"], name: "index_line_items_on_itemizable_type_and_itemizable_id"
  end

  create_table "rentals", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.bigint "reservation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "item_id"
    t.integer "duration_type", default: 0, null: false
    t.index ["item_id"], name: "index_rentals_on_item_id"
    t.index ["reservation_id"], name: "index_rentals_on_reservation_id"
  end

  create_table "reservations", force: :cascade do |t|
    t.string "name", null: false
    t.string "additional_notes"
    t.bigint "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "event_date"
    t.string "event_location"
    t.boolean "power_on_site", default: false, null: false
    t.boolean "water_on_site", default: false, null: false
    t.string "number_of_guests"
    t.string "event_type"
    t.integer "status", default: 0, null: false
    t.index ["customer_id"], name: "index_reservations_on_customer_id"
  end

  create_table "seo_locations", force: :cascade do |t|
    t.string "name", null: false
    t.string "slug", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["slug"], name: "index_seo_locations_on_slug", unique: true
  end

  create_table "service_types", force: :cascade do |t|
    t.string "name", null: false
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "price", precision: 16, scale: 2, default: "0.0", null: false
  end

  create_table "services", force: :cascade do |t|
    t.bigint "reservation_id"
    t.bigint "service_type_id"
    t.string "name", null: false
    t.string "description"
    t.decimal "price", precision: 16, scale: 2, default: "0.0", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reservation_id"], name: "index_services_on_reservation_id"
    t.index ["service_type_id"], name: "index_services_on_service_type_id"
  end

  create_table "singular_page_contents", force: :cascade do |t|
    t.text "content"
    t.string "section"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trailer_interior_features", force: :cascade do |t|
    t.string "feature", null: false
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trailer_optional_features", force: :cascade do |t|
    t.string "feature", null: false
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", null: false
    t.string "encrypted_password", limit: 128, null: false
    t.string "confirmation_token", limit: 128
    t.string "remember_token", limit: 128, null: false
    t.boolean "admin", default: false, null: false
    t.index ["email"], name: "index_users_on_email"
    t.index ["remember_token"], name: "index_users_on_remember_token"
  end

  add_foreign_key "invoices", "reservations"
  add_foreign_key "line_items", "invoices"
  add_foreign_key "services", "reservations"
  add_foreign_key "services", "service_types"
end
