class AddIndicesToCustomer < ActiveRecord::Migration[5.1]
  def change
    add_index :customers, :first_name
    add_index :customers, :last_name
    add_index :customers, :email
    add_index :customers, :phone
  end
end
