class ServicesController < ApplicationController
  before_action :require_login
  before_action :require_admin

  def new
    @service = Service.new(new_service_params)
  end

  def create
    @service = Service.new(service_params)

    if @service.save
      redirect_to new_service_line_item_path(@service)
    else
      render :new
    end
  end

  def edit
    @service = Service.find(params[:id])
  end

  def update
    @service = Service.find(params[:id])
    if @service.update(service_params)
      if @service.line_item.present?
        redirect_to edit_line_item_path(@service.line_item)
      else
        redirect_to new_service_line_item_path(@service)
      end
    else
      render :edit
    end
  end

  def destroy
    @service = Service.find(params[:id])
    if @service.destroy
      flash[:notice] = 'Service has been deleted'
      redirect_to @service.reservation
    end
  end

  private

  def service_params
    params.require(:service).permit(:reservation_id, :service_type_id, :name, :description, :price)
  end

  def new_service_params
    params.permit(:reservation_id)
  end
end
