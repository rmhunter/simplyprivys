class DashboardsController < ApplicationController
  before_action :require_login
  before_action :require_admin

  def show
    @dashboard = Dashboard.new
  end

end
