require 'rails_helper'

RSpec.describe FrequentlyAskedQuestion, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:question) }
    it { should validate_presence_of(:answer) }
  end
end

