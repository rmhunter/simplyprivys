class AddStatusToReservations < ActiveRecord::Migration[5.1]
  def change
    add_column :reservations, :status, :integer, null: false, default: 0, index: true
  end
end
