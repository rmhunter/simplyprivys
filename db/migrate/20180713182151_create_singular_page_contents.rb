class CreateSingularPageContents < ActiveRecord::Migration[5.1]
  def change
    create_table :singular_page_contents do |t|
      t.text :content
      t.string :section

      t.timestamps
    end
  end
end
