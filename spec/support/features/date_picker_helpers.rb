module Features
  module DatePickerHelpers
    def fill_in_date_picker(id, with:)
      page.execute_script("$('##{id}').val('#{with.to_s}');");
    end
  end
end

RSpec.configure do |config|
  config.include Features::DatePickerHelpers, type: :feature
end
