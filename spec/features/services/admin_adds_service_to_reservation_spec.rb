require 'rails_helper'

RSpec.feature 'Admin adds a service to a reservation' do
  scenario 'sees form for new service' do
    reservation = create(:reservation)

    visit reservation_path(reservation, as: create(:admin_user))
    click_on 'Add Service'

    expect(current_path).to eq new_reservation_service_path(reservation)
    expect(page).to have_content('Service type')
  end

  scenario 'sees pre-populated line item form after saving service' do
    reservation = create(:reservation)
    service_type = create(:service_type)

    visit reservation_path(reservation, as: create(:admin_user))
    click_on 'Add Service'
    select service_type.name, from: 'service_service_type_id'
    click_on 'Continue'

    service = Service.last
    expect(current_path).to eq new_service_line_item_path(service)
    expect(find_field('line_item_description').value).to eq service.billing_description
    expect(find_field('line_item_quantity').value).to eq service.billing_quantity.to_s
    expect(find_field('line_item_price').value).to eq service.billing_price.to_s
    expect(find_field('line_item_discount_percent').value).to eq '0.0'
    expect(find_field('line_item_tax_percent').value).to eq '8.45'
  end

  scenario 'successfully creates both service and line item' do
    reservation = create(:reservation)
    service_type = create(:service_type)

    visit reservation_path(reservation, as: create(:admin_user))
    click_on 'Add Service'
    select service_type.name, from: 'service_service_type_id'
    click_on 'Continue'
    # now we're on the line_items#new form
    fill_in 'line_item_discount_percent', with: '25'
    click_on 'Save'

    service = Service.last
    expect(current_path).to eq reservation_path(service.reservation)
    expect(page).to have_content service.line_item.description
    expect(service.line_item.discount_percent).to eq 25
    expect(service.line_item.description).to eq service.billing_description
  end
end
