require 'rails_helper'

RSpec.describe SeoLocation, type: :model do
  describe 'validations' do
    it { should validate_presence_of :name }
  end

  it 'saves a slug based on its name' do
    seo_location = build(:seo_location, name: 'Example City, USA')

    seo_location.save

    expect(seo_location.slug).to eq 'example-city-usa'
  end

  it 'updates the slug when the name is updated' do
    seo_location = create(:seo_location, name: 'Example City, USA')

    seo_location.name = 'New Name'
    seo_location.save

    expect(seo_location.slug).to eq 'new-name'
  end
end
