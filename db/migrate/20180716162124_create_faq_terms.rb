class CreateFaqTerms < ActiveRecord::Migration[5.1]
  def change
    create_table :faq_terms do |t|
      t.text :question, null: false
      t.text :answer, null: false
      t.integer :position

      t.timestamps
    end
  end
end
