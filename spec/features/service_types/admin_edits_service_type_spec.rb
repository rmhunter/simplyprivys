require 'rails_helper'

RSpec.feature "Admin edits a service type" do
  scenario "successfully from index" do
    service_type = create(:service_type)

    visit admin_service_types_path(as: create(:admin_user))
    edit_button(service_type).click
    fill_in 'service_type[name]', with: 'New ServiceType name'
    click_on 'Save Service Type'

    expect(page).to have_content 'ServiceType was successfully updated'
    expect(ServiceType.last.name).to eq 'New ServiceType name'
  end

  scenario "successfully from show" do
    service_type = create(:service_type)

    visit admin_service_types_path(as: create(:admin_user))
    click_on service_type.name
    edit_button(service_type).click
    fill_in 'service_type[name]', with: 'New ServiceType name'
    click_on 'Save Service Type'

    expect(page).to have_content 'ServiceType was successfully updated'
    expect(ServiceType.last.name).to eq 'New ServiceType name'
  end

  def edit_button(service_type)
    find("a[href='#{edit_admin_service_type_path(service_type)}']")
  end
end
