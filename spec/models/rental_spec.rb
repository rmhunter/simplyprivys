require 'rails_helper'

RSpec.describe Rental, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:item) }
    it { should define_enum_for(:duration_type).with([:daily, :weekly, :monthly]) }
  end

  describe 'associations' do
    it { should belong_to(:item) }
    it { should belong_to(:reservation) }
    it { should have_one(:line_item) }
  end

  describe '#billing_description' do
    it 'returns an invoice-friendly description for the rental' do
      rental = build(:rental)
      expect(rental.billing_description).to eq "Rental: #{rental.item.name} (#{rental.duration_type})"
    end
  end

  describe '#billing_quantity' do
    it 'defaults to the number of days rented (rounded)' do
      rental = build(:rental, start_time: Time.current, end_time: 2.days.from_now)
      expect(rental.billing_quantity).to eq 2
    end

    it 'is always a minimum of 1 day' do
      rental = build(:rental, start_time: Time.current, end_time: 1.hour.from_now)
      expect(rental.billing_quantity).to eq 1
    end
  end

  describe '#billing_price' do
    it 'defaults to the price-per-day of the associated item' do
      rental = build(:rental)
      expect(rental.billing_price).to eq rental.item.price_per_day
    end
  end

  describe '#billing_tax_rate' do
    it 'defaults to the system sales tax rate' do
      rental = build(:rental)
      expect(rental.billing_tax_rate).to eq 8.45
    end
  end
end
