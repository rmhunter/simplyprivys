class AddPowerAndWaterToReservations < ActiveRecord::Migration[5.1]
  def change
    add_column :reservations, :power_on_site, :boolean, null: false, default: false
    add_column :reservations, :water_on_site, :boolean, null: false, default: false
  end
end
