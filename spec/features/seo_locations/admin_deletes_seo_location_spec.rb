require 'rails_helper'

RSpec.feature "Admin deletes an SEO Location", js: true do
  scenario "successfully" do
    seo_location = create(:seo_location)

    visit admin_seo_locations_path(as: create(:admin_user))
    accept_confirm do
      delete_button(seo_location).click
    end

    expect(page).to have_content 'SeoLocation was successfully destroyed'
  end

  def delete_button(seo_location)
    within "tr[data-url='#{admin_seo_location_path(seo_location)}']" do
      find('[data-method=delete]')
    end
  end
end