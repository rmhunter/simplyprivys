# Delivery Business Application

- Rails version 5.1.6
- Ruby version 2.5.0
- Postgresql 9.5+


### Development Environment Setup

To get a local development environment up and running:

1. Clone the repository, `cd` into the directory, and install dependencies:

        bundle install

2. Create local postgres user

        createuser --createdb --login -P deliverybusiness
        # when prompted for password, use `deliverybusiness` (same as user)
        
3. Create and migrate the db:

        bundle exec rails db:create
        bundle exec rails db:migrate
    
4. Run the app using Foreman:

        foreman start

Open <http://localhost:5000/> and you should see the running application.


### How this app was built

This is a log, which I will eventually extract into notes, documenting the configuration of this app.

1. Generate the rails app

        rails new delivery-business --skip-turbolinks --skip-test -d postgresql
        
2. Configure database

        createuser --createdb --login -P deliverybusiness
        # when prompted for password, use `deliverybusiness` (same as user)
        bundle exec rails db:create
        bundle exec rails db:migrate
        
3. Install `gem 'webpacker'`, then run:

        bundle exec rails webpacker:install
        
4. Configure Foreman to run both the rails server and the webpacker dev server at once via the `Procfile`

5. Install bootstrap via yarn

6. Create a root route (required to deploy) and deploy to Heroku.


##

Goal is to be Angular-ready. We won't install Angular yet, but we will install frontend dependencies via Yarn,
so that we are ready to plug in Angular and webpacker as soon as we are ready to.

##

######TODO:
Use the `bin/setup` script instead of instructions here.
