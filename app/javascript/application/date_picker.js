global.flatpickr = require('flatpickr');

$(document).ready(function() {

    // initialize any date or date/time pickers on page load
    flatpickr('.date_picker', {});
    flatpickr('.date_time_picker', {
        enableTime: true,
    });

});
