require 'rails_helper'

RSpec.feature "Admin sees a user" do
  scenario "successfully" do
    user = create(:user)

    visit admin_users_path(as: create(:admin_user))
    click_on user.email

    expect(page).to have_content user.email
  end
end