module Admin
  class FrequentlyAskedQuestionsController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = FrequentlyAskedQuestion.
    #     page(params[:page]).
    #     per(10)
    # end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   FrequentlyAskedQuestion.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information
    def index
      super
      @resources = FrequentlyAskedQuestion.all


    end

    def sort
      params[:frequently_asked_question].each_with_index do |id, index|
        FrequentlyAskedQuestion.where(id: id).update_all(position: index + 1)
      end
      head :ok
    end
  end
end
