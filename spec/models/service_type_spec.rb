require 'rails_helper'

RSpec.describe ServiceType, type: :model do
  describe 'validations' do
    it { should validate_presence_of :name }
  end

  describe 'associations' do
  end
end
