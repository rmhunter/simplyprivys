require 'rails_helper'

RSpec.feature "Admin creates an item" do
  scenario "successfully" do
    visit admin_items_path(as: create(:admin_user))

    click_on 'New item'
    fill_in 'item[name]', with: 'Item 1'
    click_on 'Save Item'

    expect(page).to have_content 'Item was successfully created'
    expect(page).to have_content 'Item 1'
    expect(Item.last.name).to eq 'Item 1'
  end
end