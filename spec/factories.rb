FactoryBot.define do

  sequence :email do |n|
    "person#{n}@example.com"
  end

  sequence :name do |n|
    "Name #{n}"
  end

  factory :user do
    email
    password "password"

    factory :admin_user do
      admin true
    end
  end

  factory :customer do
    email

    trait :with_all_fields do
      first_name 'Joe'
      last_name 'Dirt'
      phone '123-456-7890'
    end

    trait :invalid do
      email ''
    end
  end

  factory :reservation do
    name
    customer
    invoice
  end

  factory :reservation_form do
    customer_id { create(:customer, first_name: 'George').id }

    trait :with_new_customer do
      customer_id nil
      customer_first_name 'Bob'
      customer_last_name 'McTest'
      customer_email 'bob@example.com'
      customer_phone '555-555-5555'
    end
  end

  factory :inquiry do
    first_name 'John'
    last_name 'Doe'
    email
    phone '555-555-5555'

    trait :invalid do
      email ''
    end

    trait :with_all_fields do
      company_name 'Awesome Company'
      event_date { 7.days.from_now }
      event_type 'Retreat'
      event_location 'My Location'
      number_of_guests '50'
      additional_notes 'Gonna be a wild crowd'
    end
  end

  factory :item do
    name
    price_per_day 50.00

    trait :invalid do
      name ''
    end
  end

  factory :rental do
    reservation
    item

    trait :future do
      start_time 2.days.from_now.change(hour: 9)
      end_time 3.days.from_now.change(hour: 17)
    end

    trait :past do
      start_time 5.days.ago.change(hour: 9)
      end_time 3.days.ago.change(hour: 17)
    end
  end

  factory :service_type do
    name
    price 100.00

    trait :with_all_fields do
      description 'A service type description'
    end

    trait :invalid do
      name ''
    end
  end

  factory :service do
    reservation
    service_type
    name { service_type.name }
    price { service_type.price }
  end

  factory :seo_location do
    name

    trait :invalid do
      name ''
    end
  end

  factory :invoice do
    status 0

    trait :with_discount do
      discount_percent 50.00
    end

    trait :with_all_fields do
      reservation
      due_on { 15.days.from_now }
      sent_at { Time.current }
      description "An invoice description"
    end
  end

  factory :line_item do
    invoice
    quantity { rand 1..5 }
    price { rand 50..99 }
    discount_percent 0.00
    tax_percent 0.00

    trait :with_all_fields do
      completed_on { 2.days.ago }
      description "An invoice line item"
    end

    trait :with_sales_tax do
      tax_percent 8.45
    end

    trait :with_discount do
      discount_percent 25.00
    end
  end

  factory :singular_page_content do
    content "Section Content"
    section "MySection"
  end

  factory :visitor_page_content do
    phone '123-456-7890'
    email 'info@example.com'
    website_header 'Website header'
    website_subheader 'Website subheader'
    how_it_works_header 'How it works header'
    contact_header 'Contact header'
  end

  factory :contract_term do
    term "Ok, we'll go deliver this crate like professionals, and then we'll go ride the bumper cars."
  end

  factory :frequently_asked_question do
    question "Question"
    answer "Answer"
  end

  factory :trailer_interior_feature do
    feature "Interior Feature"
  end

  factory :trailer_optional_feature do
    feature "Optional Feature"
  end
end
