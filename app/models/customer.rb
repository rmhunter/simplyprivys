class Customer < ApplicationRecord
  acts_as_paranoid

  has_many :reservations

  def to_s
    [full_name, email].compact.join(' - ')
  end

  def full_name
    [first_name, last_name].compact.join(' ')
  end

  def name_for_reservation
    company_name || full_name
  end
end
