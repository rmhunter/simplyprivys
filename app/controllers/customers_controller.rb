class CustomersController < ApplicationController
  before_action :require_login
  before_action :require_admin

  def index
    @customers = Search.new(search_params).results.page(params[:page])
  end

  def show
    @customer = Customer.with_deleted.find(params[:id])
  end

  def new
    @customer = Customer.new
  end

  def edit
    @customer = Customer.with_deleted.find(params[:id])
  end

  def create
    @customer = Customer.new(customer_params)
    if @customer.save
      flash[:notice] = 'Customer has been created'
      redirect_to @customer
    else
      flash[:danger] = 'Customer has not been created'
      render 'new'
    end
  end

  def update
    @customer = Customer.with_deleted.find(params[:id])
    if @customer.update(customer_params)
      flash[:notice] = 'Customer has been updated'
      redirect_to @customer
    else
      flash[:danger] = 'Customer has not been updated'
      render 'edit'
    end
  end

  def destroy
    @customer = Customer.find(params[:id])
    if @customer.destroy
      flash[:notice] = 'Customer has been deleted'
      redirect_to customers_path
    end
  end

  private

  def customer_params
    params.require(:customer).permit(:first_name, :last_name, :email, :phone, :company_name)
  end

  def search_params
    params.permit(:search_term, :sort_by, :sort_order)
  end
end
