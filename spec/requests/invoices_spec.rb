require 'rails_helper'
require 'support/requests/clearance_helpers'

RSpec.describe "Invoices Requests", type: :request do

  describe 'GET /invoices' do
    it 'responds success with a list of invoices' do
      invoices = create_list(:invoice, 2)

      sign_in_as create(:admin_user)
      get "/invoices"

      expect(response.status).to eq 200
      expect(assigns(:invoices).count).to eq 2
      expect(assigns(:invoices)).to include invoices.first
      expect(assigns(:invoices)).to include invoices.last
      expect(response).to render_template :index
    end

    it 'requires authenticated user' do
      invoice = create(:invoice)
      get "/invoices/#{invoice.id}"
      expect_response_to_require_login
    end
  end

  describe 'GET /invoices/:id' do
    it 'responds success with the requested invoice' do
      invoice = create(:invoice)

      sign_in_as create(:admin_user)
      get "/invoices/#{invoice.id}"

      expect(response.status).to eq 200
      expect(assigns(:invoice)).to eq invoice
      expect(response).to render_template :show
    end

    it 'requires authenticated user' do
      invoice = create(:invoice)
      get "/invoices/#{invoice.id}"
      expect_response_to_require_login
    end
  end

  describe 'GET /reservations/:reservation_id/invoices/new' do
    it 'responds success with a new invoice form for a reservation' do
      reservation = create(:reservation)

      sign_in_as(create(:admin_user))
      get "/reservations/#{reservation.id}/invoices/new"

      expect(response.status).to eq 200
      expect(assigns(:invoice)).to be_a_new Invoice
      expect(assigns(:invoice).reservation).to eq reservation
      expect(response).to render_template :new
    end

    it 'requires authenticated user' do
      reservation = create(:reservation)
      get "/reservations/#{reservation.id}/invoices/new"
      expect_response_to_require_login
    end
  end

  describe 'POST /invoices' do
    context 'with valid parameters' do
      it 'creates the invoice and redirects to the invoice' do
        valid_attributes = build(:invoice).attributes.compact

        sign_in_as(create(:admin_user))
        post '/invoices', params: { invoice: valid_attributes }

        invoice = Invoice.last
        expect(invoice).to have_attributes(valid_attributes)
        expect(Invoice.count).to eq 1
        expect(response).to redirect_to invoice
      end
    end

    it 'requires authenticated user' do
      post '/invoices', params: { invoice: attributes_for(:invoice).compact }
      expect_response_to_require_login
    end
  end
end
