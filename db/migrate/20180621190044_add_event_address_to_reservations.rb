class AddEventAddressToReservations < ActiveRecord::Migration[5.1]
  def change
    add_column :reservations, :event_location, :string
  end
end
