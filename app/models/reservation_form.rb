class ReservationForm
  include ActiveModel::Model
  include Virtus.model(nullify_blank: true)

  attribute :customer_id, Integer
  attribute :customer_first_name, String
  attribute :customer_last_name, String
  attribute :customer_email, String
  attribute :customer_phone, String
  attribute :event_date, Date
  attribute :event_type, String
  attribute :event_location, String
  attribute :number_of_guests, String
  attribute :power_on_site, Boolean
  attribute :water_on_site, Boolean
  attribute :additional_notes, String

  attr_reader :customer, :reservation

  validate :must_have_customer_input

  def initialize(attributes = {})
    @customer_id = attributes.delete(:customer_id)
    @customer = Customer.find(customer_id) if @customer_id.present?
    super
  end

  def save
    @customer = Customer.find(customer_id) if @customer_id.present?
    if valid?
      ActiveRecord::Base.transaction do
        @customer = Customer.create!(customer_attributes) if customer_id.blank? && customer_attributes.present?
        @reservation = Reservation.create!(reservation_attributes)
      end
      true
    else
      false
    end
  end
  alias save! save

  def new_record?
    true
  end

  private

  def must_have_customer_input
    if customer_id.blank? && customer_attributes.blank?
      errors.add(:customer_id, 'must select or create a customer')
      errors.add(:customer_first_name, 'must select or create a customer')
    end
  end

  def customer_attributes
    {first_name: customer_first_name, last_name: customer_last_name, email: customer_email, phone: customer_phone}
      .reject{ |k, v| v.blank? }
  end

  def reservation_attributes
    {name: default_reservation_name, customer: customer, event_date: event_date, event_type: event_type,
     event_location: event_location, number_of_guests: number_of_guests, power_on_site: power_on_site,
     water_on_site: water_on_site, additional_notes: additional_notes}
      .reject{ |k, v| v.blank? }
  end

  def default_reservation_name
    reservation_name_parts = []
    reservation_name_parts << customer.full_name if customer.present?
    reservation_name_parts << event_type if event_type.present?
    reservation_name_parts.present? ? reservation_name_parts.join(' ') : 'New Reservation'
  end
end
