require "rails_helper"

RSpec.feature "Visitor sees landing page for an SEO location" do
  scenario "sees landing page content" do
    seo_location = create(:seo_location, name: 'Example City, USA')
    visit '/restroom-rentals/example-city-usa'

    expect(page).to have_css '#hero'
    expect(page).to have_css '#how_it_works'
    expect(page).to have_css '#trailers'
    expect(page).to have_css '#faq'
    expect(page).to have_css '#contact_us'
  end

  scenario "sees SEO location name in the page title" do
    seo_location = create(:seo_location, name: 'Example City, USA')
    visit '/restroom-rentals/example-city-usa'

    expect(page.title).to have_content seo_location.name
  end

  scenario "sees SEO location name in the hero text" do
    seo_location = create(:seo_location, name: 'Example City, USA')
    visit '/restroom-rentals/example-city-usa'

    within '#hero' do
      expect(page).to have_content seo_location.name
    end
  end

  scenario "sees SEO location name slug in the URL" do
    seo_location = create(:seo_location, name: 'Example City, USA')
    visit '/restroom-rentals/example-city-usa'

    expect(current_url).to include seo_location.slug
  end
end
