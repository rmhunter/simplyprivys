class Search
  def initialize(params = {})
    @search_term = params.delete(:search_term)
    @sort_by = params.delete(:sort_by)
    @sort_order = params.delete(:sort_order)
  end

  def run
    CustomerSearchQuery.new(search_term: search_term, sort_by: sort_by, sort_order: sort_order).to_relation
  end

  alias results run

  private

  attr_reader :search_term, :sort_by, :sort_order
end
