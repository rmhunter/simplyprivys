require 'rails_helper'
require 'support/requests/clearance_helpers'

RSpec.describe 'Dashboards Endpoints', type: :request do

  describe 'GET /dashboard' do
    it "responds success" do
      sign_in_as(create(:admin_user))
      get '/dashboard'

      expect(response.status).to eq 200
      expect(response).to render_template :show
    end

    it "responds with reservations for today's date" do
      reservation = create(:reservation, event_date: DateTime.now)

      sign_in_as(create(:admin_user))
      get '/dashboard'
      dashboard = assigns(:dashboard)

      expect(dashboard.reservations_today).to include reservation
    end

    it "responds with upcoming reservations" do
      reservation = create(:reservation, event_date: DateTime.tomorrow)

      sign_in_as(create(:admin_user))
      get '/dashboard'
      dashboard = assigns(:dashboard)

      expect(dashboard.reservations_upcoming).to include reservation
    end

    it "responds with reservations whos status is inquired" do
      reservation = create(:reservation, status: 1)

      sign_in_as(create(:admin_user))
      get '/dashboard'
      dashboard = assigns(:dashboard)

      expect(dashboard.reservations_inquiring).to include reservation
    end

    it 'requires authenticated user' do
      get '/dashboard'
      expect_response_to_require_login
    end
  end
end