class RenameServiceToServiceType < ActiveRecord::Migration[5.1]
  def change
    rename_table :services, :service_types
  end
end
