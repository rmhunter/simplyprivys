require 'rails_helper'

RSpec.describe Invoice, type: :model do
  describe 'validations' do
    it { should define_enum_for(:status).with([:created, :sent, :paid, :void]) }
    it { should validate_presence_of(:status) }
  end

  describe 'associations' do
    it { should belong_to :reservation }
    it { should have_many(:line_items).dependent(:destroy) }
  end

  describe 'nested attributes' do
    it { should accept_nested_attributes_for(:line_items).allow_destroy(true) }
  end

  describe 'scopes' do
    describe 'default' do
      it 'defaults to created_at descending' do
        older_invoice = create(:invoice)
        newer_invoice = create(:invoice)

        expect(Invoice.first).to eq newer_invoice
        expect(Invoice.last).to eq older_invoice
      end
    end
  end

  describe 'pre_tax_subtotal' do
    it 'returns the sum of all line items before tax' do
      invoice = create(:invoice)
      create_list(:line_item, 3, invoice: invoice)
      expect(invoice.pre_tax_subtotal).to eq(invoice.line_items.reduce(0){|sum, line_item| sum + line_item.pre_tax_subtotal})
    end
  end

  describe 'tax_amount' do
    it 'returns the sum of all line item tax amounts' do
      invoice = create(:invoice)
      create_list(:line_item, 3, :with_sales_tax, invoice: invoice)
      expect(invoice.tax_amount).to eq(invoice.line_items.reduce(0){|sum, line_item| sum + line_item.tax_amount})
    end
  end

  describe 'total' do
    it 'returns the pre-tax subtotal plus the tax amount' do
      invoice = create(:invoice)
      create_variety_of_line_items_for invoice
      expect(invoice.total).to eq invoice.pre_tax_subtotal + invoice.tax_amount
    end

    it 'equals the sum of line items\' totals' do
      invoice = create(:invoice)
      create_variety_of_line_items_for invoice
      sum_of_line_item_totals = invoice.line_items.reduce(0){|sum, line_item| sum + line_item.total}
      expect(invoice.total).to eq sum_of_line_item_totals
    end

    it 'returns zero if no line items' do
      invoice = create(:invoice)
      expect(invoice.total).to eq 0
    end
  end

  describe 'calculations' do
    it 'handles basic line items' do
      invoice = create(:invoice)
      create(:line_item, invoice: invoice, quantity: 2, price: 10)
      create(:line_item, invoice: invoice, quantity: 1, price: 5)

      expect(invoice.pre_tax_subtotal).to eq 25
      expect(invoice.tax_amount).to eq 0
      expect(invoice.total).to eq 25
    end

    it 'handles discounts' do
      invoice = create(:invoice)
      create(:line_item, invoice: invoice, quantity: 1, price: 50, discount_percent: 0)
      create(:line_item, invoice: invoice, quantity: 2, price: 10, discount_percent: 10)
      create(:line_item, invoice: invoice, quantity: 3, price: 5, discount_percent: 50)
      create(:line_item, invoice: invoice, quantity: 4, price: 1, discount_percent: 100)

      expect(invoice.pre_tax_subtotal).to eq (50 + 18 + 7.5 + 0)
      expect(invoice.tax_amount).to eq (0 + 0 + 0 + 0)
      expect(invoice.total).to eq (50 + 18 + 7.5 + 0)
    end

    it 'handles taxes' do
      invoice = create(:invoice)
      create(:line_item, invoice: invoice, quantity: 1, price: 50, tax_percent: 0)
      create(:line_item, invoice: invoice, quantity: 2, price: 10, tax_percent: 10)

      expect(invoice.pre_tax_subtotal).to eq (50 + 20)
      expect(invoice.tax_amount).to eq (0 + 2)
      expect(invoice.total).to eq (50 + 22)
    end

    it 'handles a variety of discounts and taxes' do
      invoice = create(:invoice)
      create(:line_item, invoice: invoice, quantity: 1, price: 50, discount_percent: 10, tax_percent: 0)
      create(:line_item, invoice: invoice, quantity: 2, price: 10, discount_percent: 0, tax_percent: 10)
      create(:line_item, invoice: invoice, quantity: 3, price: 5, discount_percent: 0, tax_percent: 0)
      create(:line_item, invoice: invoice, quantity: 4, price: 1, discount_percent: 50, tax_percent: 10)

      expect(invoice.pre_tax_subtotal).to eq (45 + 20 + 15 + 2)
      expect(invoice.tax_amount).to eq (0 + 2 + 0 + 0.20)
      expect(invoice.total).to eq (45 + 22 + 15 + 2.20)
    end
  end

  def create_variety_of_line_items_for(invoice)
    create(:line_item, invoice: invoice)
    create(:line_item, :with_discount, invoice: invoice)
    create(:line_item, :with_sales_tax, invoice: invoice)
    create(:line_item, :with_discount, :with_sales_tax, invoice: invoice)
  end
end
