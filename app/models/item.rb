class Item < ApplicationRecord
  has_many :rentals
  validates :name, presence: true
  validates :price_per_day, presence: true, numericality: true
end
