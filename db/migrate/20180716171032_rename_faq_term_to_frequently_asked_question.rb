class RenameFaqTermToFrequentlyAskedQuestion < ActiveRecord::Migration[5.1]
  def change
    rename_table :faq_terms, :frequently_asked_questions
  end
end
