require 'rails_helper'

RSpec.feature "Admin sees service type index page" do
  scenario "successfully" do
    service_types = create_list(:service_type, 2)

    visit admin_service_types_path(as: create(:admin_user))

    expect(page).to have_content(service_types.first.name)
    expect(page).to have_content(service_types.second.name)
  end
end