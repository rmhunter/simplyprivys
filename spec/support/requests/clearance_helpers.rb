module Requests
  module ClearanceHelpers
    def sign_in
      user = create(:user)
      sign_in_as(user)
    end

    def sign_in_as(user)
      post '/session', params: { session: { email: user.email, password: user.password } }
    end

    def expect_response_to_require_login
      expect(response).to redirect_to sign_in_path
    end
  end
end

RSpec.configure do |config|
  config.include Requests::ClearanceHelpers, type: :request
end
