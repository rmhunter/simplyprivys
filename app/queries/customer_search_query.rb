class CustomerSearchQuery
  def initialize(search_term:, sort_by:, sort_order:)
    @search_term = search_term
    @sort_by = sort_by
    @sort_order = sort_order
  end

  def to_relation
    scope.order("#{sanitized_sort_by} #{sanitized_sort_order}")
  end

  private

  attr_reader :search_term, :sort_by, :sort_order

  def scope
    where_clause = searchable_fields.map{|f| "#{f} ILIKE :q"}.join(' OR ')
    search_term.present? ? Customer.where(where_clause, q: "%#{search_term}%") : Customer.all
  end

  def searchable_fields
    %w[first_name last_name email phone]
  end

  def sortable_fields
    %w[first_name last_name email phone created_at updated_at]
  end

  def sanitized_sort_order
    if sort_order.present? && %w[DESC ASC].include?(sort_order.upcase)
      sort_order.upcase
    else
      default_sort_order
    end
  end

  def default_sort_order
    'ASC'
  end

  def sanitized_sort_by
    sortable_fields.include?(sort_by) ? sort_by : default_sort_by
  end

  def default_sort_by
    'first_name'
  end
end
