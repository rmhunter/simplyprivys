class AddPricePerDayToItems < ActiveRecord::Migration[5.1]
  def change
    add_column :items, :price_per_day, :decimal,
               precision: 16, scale: 2, null: false, default: 0
  end
end
