require 'rails_helper'

RSpec.describe Inquiry, type: :model do
  describe "validations" do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:phone) }
  end

  describe "associations" do
    it "belongs to a Customer" do
      expect(create(:inquiry).customer).to be_a Customer
    end
    it "has a Reservation" do
      expect(create(:inquiry).reservation).to be_a Reservation
    end
  end

  describe "#save" do
    it "finds/creates a customer with given email" do
      inquiry = build(:inquiry, :with_all_fields)

      inquiry.save

      customer = Customer.last
      expect(customer.first_name).to eq inquiry.first_name
      expect(customer.last_name).to eq inquiry.last_name
      expect(customer.email).to eq inquiry.email
      expect(customer.phone).to eq inquiry.phone
      expect(customer.company_name).to eq inquiry.company_name
    end

    it "saves a reservation and associates it with the customer" do
      inquiry = build(:inquiry, :with_all_fields)

      inquiry.save

      reservation = Reservation.last
      customer = Customer.find_by(email: inquiry.email)
      expect(reservation.customer).to eq customer
      expect(reservation.name).to eq "#{inquiry.company_name} #{inquiry.event_type}"
      expect(reservation.event_date).to eq inquiry.event_date
      expect(reservation.event_type).to eq inquiry.event_type
      expect(reservation.event_location).to eq inquiry.event_location
      expect(reservation.number_of_guests).to eq inquiry.number_of_guests
      expect(reservation.additional_notes).to eq inquiry.additional_notes
    end

    it "sends emails to customer and admin" do
      inquiry = build(:inquiry, :with_all_fields)

      expect { inquiry.save }
          .to change { ActionMailer::Base.deliveries.count }.by(2)
    end

    it "updates previously-null fields on Customer if it already existed" do
      customer = create(:customer)
      inquiry = build(:inquiry, first_name: 'Bob', last_name: 'Smith',
                      email: customer.email, phone: '123-456-7890',
                      company_name: 'Awesome Company')

      inquiry.save

      expect(inquiry.customer.first_name).to eq 'Bob'
      expect(inquiry.customer.last_name).to eq 'Smith'
      expect(inquiry.customer.phone).to eq '123-456-7890'
      expect(inquiry.customer.company_name).to eq 'Awesome Company'
    end
  end

  describe "#send_customer_receipt" do
    it "delivers an email" do
      inquiry = create(:inquiry, :with_all_fields)

      expect { inquiry.send_customer_receipt }
          .to change { ActionMailer::Base.deliveries.count }.by(1)
    end
  end

  describe "#send_admin_notification" do
    it "delivers an email" do
      inquiry = create(:inquiry, :with_all_fields)

      expect { inquiry.send_admin_notification }
          .to change { ActionMailer::Base.deliveries.count }.by(1)
    end
  end

  describe '#default_reservation_name' do
    it 'defaults to the company name if present' do
      create(:inquiry, company_name: 'Awesome Company')
      expect(Reservation.last.name).to eq 'Awesome Company'
    end

    it 'includes the event type if present' do
      create(:inquiry, company_name: 'Awesome Company', event_type: 'Retreat')
      expect(Reservation.last.name).to eq 'Awesome Company Retreat'
    end

    it 'falls back to the customer full name' do
      create(:inquiry, first_name: 'Bob', last_name: 'Smith', company_name: nil)
      expect(Reservation.last.name).to eq 'Bob Smith'
    end
  end
end
