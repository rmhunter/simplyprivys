class SingularPageContent < ApplicationRecord
  validates :content, presence: true
  validates :section, uniqueness: true

  def self.value_for(section)
    record = where(section: section.to_s).first
    record.present? ? record.content : nil
  end
end
